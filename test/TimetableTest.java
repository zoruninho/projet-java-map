import static org.junit.Assert.*;

import java.util.Date;
import java.util.LinkedList;

import model.Line;
import model.Period;
import model.Station;
import model.Timetable;

import org.junit.*;


public class TimetableTest
{
	Timetable timetable;
	
	@Before
	public void init()
	{
		timetable = new Timetable();
	}
	
	
	
	
	@Test /* ajout d'une station à un horaire */
	public void setgetStation()
	{
		Station station = new Station(0, "station", 40, 50, "ville");
		
		timetable.setStation(station);
		
		assertEquals(station, timetable.getStation());
	}
	
	@Test
	public void setgetLine()
	{
		Line line = new Line();
		
		timetable.setLine(line);
		
		assertEquals(line, timetable.getLine());
	}
	
	@Test
	public void setgetPeriods()
	{
		LinkedList<Period> periods = new LinkedList<Period>();
		
		timetable.setPeriods(periods);
		
		assertEquals(periods, timetable.getPeriods());
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void fourPeriodString()
	{
		Date date1 = new Date(0, 0, 0, 10, 10);
		Date date2 = new Date(0, 0, 0, 11, 11);
		Date date3 = new Date(0, 0, 0, 12, 12);
		Date date4 = new Date(0, 0, 0, 13, 13);
		Date date5 = new Date(0, 0, 0, 14, 14);
		
		Period period = new Period("test");
		period.getPasses().add(date1);
		period.getPasses().add(date2);
		period.getPasses().add(date3);
		period.getPasses().add(date4);
		period.getPasses().add(date5);
		
		
		timetable.getPeriods().add(period);
						
		assertEquals("10:10 11:11 12:12 13:13 14:14 ", timetable.periodsToString("test"));
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void eightPeriodString()
	{
		Date date1 = new Date(0, 0, 0, 10, 10);
		Date date2 = new Date(0, 0, 0, 11, 11);
		Date date3 = new Date(0, 0, 0, 12, 12);
		Date date4 = new Date(0, 0, 0, 13, 13);
		Date date5 = new Date(0, 0, 0, 14, 14);
		Date date6 = new Date(0, 0, 0, 15, 15);
		Date date7 = new Date(0, 0, 0, 16, 16);
		Date date8 = new Date(0, 0, 0, 17, 17);
		Date date9 = new Date(0, 0, 0, 18, 18);
		Date date10 = new Date(0, 0, 0, 19, 19);
		
		
		Period period = new Period("test");
		period.getPasses().add(date1);
		period.getPasses().add(date2);
		period.getPasses().add(date3);
		period.getPasses().add(date4);
		period.getPasses().add(date5);
		period.getPasses().add(date6);
		period.getPasses().add(date7);
		period.getPasses().add(date8);
		period.getPasses().add(date9);
		period.getPasses().add(date10);
		
		timetable.getPeriods().add(period);
		
				
		assertEquals("10:10 11:11 12:12 13:13 14:14 \n15:15 16:16 17:17 18:18 19:19 ", timetable.periodsToString("test"));
	}
}
