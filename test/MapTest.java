import static org.junit.Assert.*;

import java.util.Date;
import java.util.LinkedList;

import model.Line;
import model.Map;
import model.Period;
import model.Station;
import model.Timetable;
import model.TransportType;

import org.junit.*;


public class MapTest {
	
	Map map;
	
	@Before
	public void init()
	{
		map = new Map();
	}
	
	@Test
	public void setgetStation()
	{
		LinkedList<Station> stations = new LinkedList<Station>();
		
		map.setStations(stations);
		
		assertEquals(stations, map.getStations());
	}
	
	@Test
	public void setgetLines()
	{
		LinkedList<Line> lines = new LinkedList<Line>();
		
		map.setLines(lines);
		
		assertEquals(lines, map.getLines());
	}
	
	@Test
	public void setgetTimetable()
	{
		LinkedList<Timetable> timetables = new LinkedList<Timetable>();
		
		map.setTimetables(timetables);
		
		assertEquals(timetables, map.getTimetables());
	}
	
	
	@Test
	public void searchScheduleByLineAndStation()
	{
		Timetable timetable1 = new Timetable();
		
		Line line1 = new Line();
		Station station1 = new Station();
		
		timetable1.setLine(line1);
		timetable1.setStation(station1);
		map.getTimetables().add(timetable1);
		
		assertEquals(1, map.searchSchedule(line1, station1).size());
		
		Timetable timetable2 = new Timetable();
		Line line2 = new Line();
		Station station2 = new Station();
		
		timetable2.setLine(line2);
		timetable2.setStation(station2);
		map.getTimetables().add(timetable2);
		
		Timetable timetable3 = new Timetable();
		Line line3 = new Line();
		Station station3 = new Station();
		
		timetable3.setLine(line3);
		timetable3.setStation(station3);
		map.getTimetables().add(timetable3);
		
		assertEquals(1, map.searchSchedule(line1, station1).size());
		
		Timetable timetable4 = new Timetable();
		timetable4.setLine(line3);
		timetable4.setStation(station3);
		map.getTimetables().add(timetable4);
		
		assertEquals(2, map.searchSchedule(line3, station3).size());
	}
	
	@Test
	public void searchScheduleByStation()
	{
		Timetable timetable1 = new Timetable();	
		Line line1 = new Line();
		Station station1 = new Station();
		
		timetable1.setLine(line1);
		timetable1.setStation(station1);
		map.getTimetables().add(timetable1);
		
		assertEquals(1, map.searchSchedule(station1).size());
		
		Timetable timetable2 = new Timetable();
		Line line2 = new Line();
		Station station2 = new Station();
		
		timetable2.setLine(line2);
		timetable2.setStation(station2);
		map.getTimetables().add(timetable2);
		
		Timetable timetable3 = new Timetable();
		Line line3 = new Line();
		Station station3 = new Station();
		
		timetable3.setLine(line3);
		timetable3.setStation(station3);
		map.getTimetables().add(timetable3);
		
		assertEquals(1, map.searchSchedule(station1).size());
		
		Timetable timetable4 = new Timetable();
		timetable4.setLine(line3);
		timetable4.setStation(station3);
		map.getTimetables().add(timetable4);
		
		assertEquals(2, map.searchSchedule(station3).size());
	}
	
	@Test
	public void emptySearchScheduleByLineAndStation()
	{
		assertEquals(0, map.searchSchedule(null, null).size());
	}
	
	@Test
	public void emptySearchScheduleByStation()
	{
		assertEquals(0, map.searchSchedule(null).size());
	}
	
	@Test
	public void findLineByName()
	{
		Line line1 = new Line("ligne1", TransportType.TRAMWAY, null);
		map.getLines().add(line1);
		
		assertEquals(line1, map.convertToLine("ligne1"));
	}
	
	@Test
	public void nullLineByName()
	{
		Line line1 = new Line("ligne1", TransportType.TRAMWAY, null);
		map.getLines().add(line1);
		
		assertNull(map.convertToLine("test"));
	}
	
	@Test
	public void nullNextArrival()
	{
		assertNull(map.getNextArrival(null, null));
	}
}
