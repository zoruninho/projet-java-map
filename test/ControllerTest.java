import java.util.LinkedList;

import model.Line;
import model.Map;
import model.Station;
import model.TransportType;

import org.junit.*;
import org.junit.rules.ExpectedException;

import view.InsertStations;
import controller.Controller;
import static org.junit.Assert.*;

public class ControllerTest {
	static Controller test;
	
	
	@Before
	public void init(){
		test = new Controller();
	}
	
	
	@Test //test de getMap et setMap
	public void testSetGetMap(){
		Map temp = new Map();
		test.setMap(temp);
		
		assertEquals(temp, test.getMap());
	}
	
	
	@Test //test de getFileName
	public void testGetFileName(){
		String temp = "./save/local.json";
		assertEquals(temp, test.getFileName());
	}
	
	@Test //test de setFileName
	public void testSetFileName(){
		String temp="./save/local.json";
		test.setFileName(temp);
		assertEquals(temp, test.getFileName());
	}
	
	@Rule public ExpectedException thrown = ExpectedException.none();
	@Test //test mauvais chemin de fichier
	public void testFichierInexistant() throws java.nio.file.NoSuchFileException{
		String temp="fichierinexistant";
		test.setFileName(temp);
	//	fail("Mauvais chemin du fichier.");
	}
	
	@Test //test getStationToPlace
	public void testGetStationToPlace(){
		InsertStations temp = test.getStationToPlace();
		assertEquals(temp, test.getStationToPlace());
	}
	
	@Test //test setStationToPlace
	public void testSetStationToPlace(){
		InsertStations temp = new InsertStations(test);
		test.setStationToPlace(temp);
		assertEquals(temp, test.getStationToPlace());
	}
	
	@Test //test de getLinesNames
	public void testGetLinesNames(){
		String[] noms = new String[] {"Nom1", "Nom2"};
		LinkedList <Line> line = new LinkedList<Line>();
		line.add(new Line(noms[0], TransportType.BUS, null));
		line.add(new Line(noms[1], TransportType.BUS, null));
		
		test.getMap().setLines(line);
		
		assertArrayEquals(noms, test.getLinesNames());
	}
	
	@Test //test de checkLineName
	public void testCheckLineName(){
		String[] noms = new String[] {"Nom1", "Nom2"};
		LinkedList <Line> line = new LinkedList<Line>();
		line.add(new Line(noms[0], TransportType.BUS, null));
		line.add(new Line(noms[1], TransportType.BUS, null));
		
		test.getMap().setLines(line);
		assertTrue(test.checkLineName(noms[0]));
	}
	
	
	@Test //test de maxMinCoordinates
	public void testMaxMinCoordinates(){
		LinkedList <Station> stations = new LinkedList<Station>();
		stations.add(new Station(5, "stat1", 0, 0, "ville1"));
		stations.add(new Station(6, "stat2", 5, 5, "ville1"));
		stations.add(new Station(7, "stat3", 6, -1, "ville2"));
		
		test.getMap().setStations(stations);
		
		double[][] maxmin = test.getMaxMinCoordinate();		
		
		assertEquals(-1, maxmin[0][0], 0);
		assertEquals(5, maxmin[1][0], 0);
		assertEquals(0, maxmin[0][1], 0);
		assertEquals(6, maxmin[1][1], 0);
	}
	
	@Test //test de getStationsNames
	public void testGetStationsNames(){
		String[] noms = new String[] {"Nom1", "Nom2", "Nom3"};
		
		
		LinkedList <Station> stations = new LinkedList<Station>();
		stations.add(new Station(5, noms[0], 0, 0, "ville1"));
		stations.add(new Station(6, noms[1], 5, 5, "ville1"));
		stations.add(new Station(7, noms[2], 6, -1, "ville2"));
		
		test.getMap().setStations(stations);
		
		assertArrayEquals(noms, test.getStationsNames());
	}
	
	@Test //test de getMaxStationNumber
	public void testGetMaxStationNumber(){
		String[] noms = new String[] {"Nom1", "Nom2", "Nom3"};
		
		LinkedList <Station> stations = new LinkedList<Station>();
		stations.add(new Station(5, noms[0], 0, 0, "ville1"));
		stations.add(new Station(6, noms[1], 5, 5, "ville1"));
		stations.add(new Station(7, noms[2], 6, -1, "ville2"));
		
		test.getMap().setStations(stations);
		
		assertEquals(7, test.getMaxStationNumber());
		
	}
	
	
	@Test //test de copyStation
	public void testCopyStation(){
		Station stat1 = new Station(0, "Nom1", 0, 0, "ville1");
		LinkedList <Station> stations = new LinkedList<Station>();
		stations.add(stat1);
		test.getMap().setStations(stations);
		
		assertEquals(stat1, test.copyStation(new Station(5684489, "Nom1", 6484, 84564984, "ville15456")));
	}
}
