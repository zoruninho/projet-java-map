import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
	FileManagerTest.class,
	LigneTest.class,
	StationTest.class,
	TimetableTest.class,
	PeriodTest.class,
	MapTest.class
})
public class AllTests {

}
