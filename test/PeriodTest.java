import static org.junit.Assert.*;

import java.util.Date;
import java.util.LinkedList;

import model.Period;

import org.junit.Before;
import org.junit.Test;


public class PeriodTest
{
	Period period;
	
	@Before
	public void init()
	{
		period = new Period();
	}
	
	
	@Test
	public void setgetPasses()
	{
		LinkedList<Date> dates = new LinkedList<Date>();
				
		
		period.setPasses(dates);
				
		assertEquals(dates, period.getPasses());
	}
	
	@Test
	public void getLabel()
	{
		period = new Period("lundi");
		
		assertEquals("lundi", period.getLabel());
	}
	
	@Test
	public void setgetLabel()
	{
		String label = "labelTest";
		
		period.setLabel(label);
		
		assertEquals(label, period.getLabel());
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void getOneStringDates()
	{
		Date date1 = new Date();
		date1.setHours(10);
		date1.setMinutes(20);
		
		String[] datesString = new String[1];
		datesString[0] = "10:20";
		
		
		period.getPasses().add(date1);
		
		assertArrayEquals(datesString, period.getDates());
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void getSeveralStringDates()
	{
		Date date1 = new Date(),
			 date2 = new Date(),
			 date3 = new Date();
		
		date1.setHours(10);
		date1.setMinutes(10);
		
		date2.setHours(11);
		date2.setMinutes(11);
		
		date3.setHours(12);
		date3.setMinutes(12);
		
		String[] stringDates = new String[3];
		
		stringDates[0] = "10:10";
		stringDates[1] = "11:11";
		stringDates[2] = "12:12";
		
		period.getPasses().add(date1);
		period.getPasses().add(date2);
		period.getPasses().add(date3);
		
		assertArrayEquals(stringDates, period.getDates());
	}
	
	
	
	
	@SuppressWarnings("deprecation")
	@Test
	public void removePasse()
	{
		Date date = new Date();
		date.setHours(12);
		date.setMinutes(30);
		
		period.getPasses().add(date);
		
		assertEquals(1, period.getPasses().size());
		
		period.removePasse("12:30");
		
		assertEquals(0, period.getPasses().size());
	}
	
}
