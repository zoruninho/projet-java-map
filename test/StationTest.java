import static org.junit.Assert.*;

import org.junit.*;

import model.*;


public class StationTest {
	static Station stat1;
	
	@BeforeClass
	public static void init (){
		stat1 = new Station(1, "Nom1", 5, 5, "Ville1");
	}

	@Test /* ajout d'une nouvelle station de base*/
	public void testCreationStation1() {
		Station test = new Station();
		assertEquals(0, test.getNumber());
		assertEquals("", test.getName());
		assertEquals(0, test.getLongitude(), 0);
		assertEquals(0, test.getLatitude(), 0);
		assertEquals("", test.getCityName());
	}
	
	@Test /* ajout d'une nouvelle station de base*/
	public void testCreationStation2(){
		Station test = new Station(1, "Station1", 2.5, 5.1, "Caen");
		assertEquals(1, test.getNumber());
		assertEquals("Station1", test.getName());
		assertEquals(5.1, test.getLongitude(), 0);
		assertEquals(2.5, test.getLatitude(), 0);
		assertEquals("Caen", test.getCityName());
	}
	
	@Test /*modification du numéro d'une station*/
	public void testModifierNumStation(){
		stat1.setNumber(10);
		assertEquals(10, stat1.getNumber());
	}

	@Test /*modification du nom d'une station*/
	public void testModifierNomStation(){
		stat1.setName("TestNom");
		assertEquals("TestNom", stat1.getName());
	}
	
	@Test /*modification de la longitude d'une station*/
	public void testModifierLongitudeStation(){
		stat1.setLongitude(10.0);
		assertEquals(10.0, stat1.getLongitude(), 0);
	}
	
	@Test /*modification de la latitude d'une station*/
	public void testModifierLatitudeStation(){
		stat1.setLatitude(10.0);
		assertEquals(10.0, stat1.getLatitude(), 0);
	}
	
	@Test /*modification de la ville d'une station*/
	public void testModifierVilleStation(){
		stat1.setCityName("Paris");
		assertEquals("Paris", stat1.getCityName());
	}
	
}
