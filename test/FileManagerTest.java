import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import model.FileManager;
import model.Map;

import org.junit.*;

public class FileManagerTest
{
		
	static String pathToRead = "./toRead.json";
	static String pathToWrite = "./toWrite.json";
	
	static String emptyJson = "{\"@type\":\"model.Map\",\"stations\":[],\"lines\":[],\"timetables\":[]}";
	
	@BeforeClass
	static public void init()
	{
		// création du fichier à lire
		try {
			Writer f = new FileWriter(pathToRead);
			f.write(emptyJson);
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	
	@Test /* essaye d'écrire dans un fichier */
	public void testWrite()
	{
		Map map = new Map();
		
		FileManager.save(pathToWrite, map);
		
		
		File file = new File(pathToWrite);
		assertTrue(file.exists());
		
		
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(pathToWrite));
			String string =  Charset.defaultCharset().decode(ByteBuffer.wrap(encoded)).toString();
			
			string = string.replaceAll("[\r\n ]","");
			
			assertEquals(emptyJson, string);
		
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test /* lecture d'un fichier de sauvegarde */
	public void testRead()
	{		
		Object map = FileManager.load(pathToRead);
		
		assertEquals("model.Map", map.getClass().getName());
	}
	
	@AfterClass
	static public void clean()
	{
		// suppresion du fichier créé
		File fileToRead = new File(pathToRead);
		fileToRead.delete();
		
		File fileToWrite = new File(pathToWrite);
		fileToWrite.delete();
	}
	

}
