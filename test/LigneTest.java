import java.util.LinkedList;

import static org.junit.Assert.*;
import org.junit.*;

import model.*;

public class LigneTest{
	static Station stat1, stat2;
	static LinkedList<Station> list;
	static Line test;
	
	@Before
	public void init (){
		list = new LinkedList<Station>();
		stat1 = new Station(1, "Nom1",5 , 5, "Ville1");
		stat2 = new Station(2, "Nom2", 6, 6, "Ville2");
		list.add(stat1);
		list.add(stat2);
		test = new Line("Ligne1", TransportType.BUS, list);
	}

	@Test /* ajout d'une nouvelle ligne */
	public void testCreationLigne() {
		
		assertArrayEquals(new String[] {"Nom1", "Nom2"}, test.getStationsNames());
		assertEquals("Ligne1", test.getName());
		assertEquals(TransportType.BUS, test.getTransportType());
	}
	
	@Test /*ajout d'une station */
	public void testAjoutStation(){
		Station stat3 = new Station(3, "Nom3", 3, 3, "Ville1");
		test.getStations().add(1, stat3);
		
		
		assertArrayEquals(new String[] {"Nom1", "Nom3", "Nom2"},
				test.getStationsNames());
	}
	
	@Test /*trouver la position d'une station*/
	public void testPositionStation(){
		assertEquals(2, test.getPositionStation("Nom2"));
	}
	
	@Test /*suppression d'une station */
	public void testSupprimerStation(){
		Station stat3 = new Station(3, "Nom3", 3, 3, "Ville1");
		test.getStations().add(1, stat3);
		//l'ajout est fonctionnel par le test ci-dessus, on ne le vérifie pas.
		
		test.getStations().remove(test.getPositionStation("Nom1")-1);
		
		assertArrayEquals(new String[] {"Nom3", "Nom2"},
				test.getStationsNames());
	}
	
	@Test /*modifier le nom d'une ligne*/
	public void testModifierNomLigne() {
		test.setName("TestNom");
		assertEquals("TestNom", test.getName());
	}
	
	@Test /*modifier le type de transport d'une ligne */
	public void testModifierTypeTransport(){
		test.setTransportType(TransportType.TRAMWAY);
		assertEquals(TransportType.TRAMWAY, test.getTransportType());
	}
	
	@Test /*retourner tous les noms des stations sauf la dernière */
	public void testRetournerToutesSaufDerniere() {
		test.getStations().add(1, new Station(3, "Nom4", 6, 6, "Ville4"));
		assertArrayEquals(new String[] {"Nom1", "Nom4"}, 
				test.getStationsNamesExceptLast());
	}
}
