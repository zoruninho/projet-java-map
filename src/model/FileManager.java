package model;


import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/**
 * La classe <b>FileManager</b> permet de sauvegarder ou charger
 * les données
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 *
 */
public class FileManager
{
	/**
	 * Charge les données du fichier de sauvegarde dans une Map
	 * 
	 * @param filePath une chaîne de caractères contenant le chemin d'accès
	 * au fichier de sauvegarde
	 * @return une map contenant les données chargées
	 */
	public static Map load(String filePath)
	{
		Map map = null;
		
		try
		{
			//Récupère tout le contenu du fichier
			byte[] encoded = Files.readAllBytes(Paths.get(filePath));
			
			//Transforme le tableau de byte en une chaîne de caractères
			String string =  Charset.defaultCharset().decode(ByteBuffer.wrap(encoded)).toString();
			
			//Transforme la chaîne de caractères en objet map
			map = (Map)JsonReader.jsonToJava(string);
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return map;
	}
	
	/**
	 * Sauvegarde les données de la map dans le fichier de sauvegarde
	 * 
	 * @param filePath une chaîne de caractères contenant le chemin d'accès
	 * au fichier de sauvegarde
	 * @param map la map contenant les données
	 */
	public static void save(String filePath, Map map)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		JsonParser parser = new JsonParser();
		
		try {
			JsonElement json = parser.parse(JsonWriter.objectToJson(map));
			
			FileWriter writer = new FileWriter(filePath);
			
			writer.write(gson.toJson(json));
			writer.close();
			
		} catch (JsonSyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
