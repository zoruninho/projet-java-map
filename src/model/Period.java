package model;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

/**
 * La classe <b>Period</b> représente une période de la semaine 
 * (week-end ou semaine) et les horaires de passages associés
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Timetable
 *
 */
public class Period {
	
	private LinkedList<Date> passes;
	private String label;
	
	/**
	 * Constructeur par défaut<br/>
	 * Initialise une période sans label ni horaires de passage
	 */
	public Period() {
		super();
		this.passes=new LinkedList<Date>();
		this.label="";
	}
	
	/**
	 * Initialise une période avec un label mais pas d'horaires de passage
	 * 
	 * @param label libellé identifiant la période
	 */
	public Period(String label) {
		super();
		this.passes=new LinkedList<Date>();
		this.label=label;
	}
	
	/**
	 * Renvoie la liste contenant les horaires de passage
	 * 
	 * @return une liste de dates
	 */
	public LinkedList<Date> getPasses() {
		return passes;
	}
	
	/**
	 * Remplace la liste des horaires de passage par une autre liste
	 * 
	 * @param passes la nouvelle liste de dates
	 */
	public void setPasses(LinkedList<Date> passes) {
		this.passes = passes;
	}
	
	/**
	 * Renvoie le label de la période
	 * 
	 * @return une chaîne de caractères identifiant la période
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * Remplace le label de la période par un autre
	 * 
	 * @param label la nouvelle chaîne de caractères
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	/**
	 * Renvoie les horaires de passage de la période
	 * 
	 * @return un tableau de chaînes de caractères
	 */
	public String[] getDates(){
		String[] dates = new String[this.passes.size()];
		DateFormat df = new SimpleDateFormat("HH:mm");
		
		for(int i=0;i<this.passes.size();i++)
		{
			dates[i]=df.format(this.passes.get(i));
		}
		
		return dates;
	}
	
	/**
	 * Supprime un horaire de la liste d'horaires
	 * 
	 * @param passe une chaîne de caractères contenant l'horaire à supprimer
	 */
	public void removePasse(String passe)
	{
		String date = new String();
		DateFormat df = new SimpleDateFormat("HH:mm");
		
		for(int i=0;i<this.passes.size();i++)
		{
			date=df.format(this.passes.get(i));
			if(date.equals(passe))
			{
				this.passes.remove(i);
			}
		}
	}

}
