package model;

/**
 * L'énumération <b>TransportType</b> contient les 3 types de transports
 * possibles : "métro", "tramway" et "bus"
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 */
public enum TransportType {
	METRO {
		public String toString() {
			return "METRO";
		}
	},
	BUS {
		public String toString() {
			return "BUS";
		}
	},
	TRAMWAY {
		public String toString() {
			return "TRAMWAY";
		}
	};

}
