package model;

import java.util.LinkedList;

/**
 * La classe <b>Timetable</b> représente les horaires liés à une station, pour
 * une ligne donnée, en fonction de la période de la semaine
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Line
 * @see Station
 * @see Period
 * 
 */
public class Timetable {

	private Line line;
	private Station station;
	private LinkedList<Period> periods;

	/**
	 * Constructeur par défaut<br/>
	 * Initialise une timetable sans lignes, ni stations mais avec une période
	 * "semaine" et une période "week-end" ne contenant pas d'horaires
	 */
	public Timetable() {
		super();
		this.line = new Line();
		this.station = new Station();
		this.periods = new LinkedList<Period>();
		periods.add(new Period("Semaine"));
		periods.add(new Period("Week-end"));
	}

	/**
	 * Crée une timetable en fonction des données d'une autre timetable
	 * 
	 * @param temp la timetable dont les données seront copiées dans la nouvelle
	 * timetable
	 */
	public Timetable(Timetable temp) {
		super();
		this.line = temp.line;
		this.station = temp.station;
		this.periods = temp.periods;
	}

	/**
	 * Renvoie la ligne liée à la timetable
	 * 
	 * @return une ligne de type Line
	 */
	public Line getLine() {
		return line;
	}

	/**
	 * Remplace la ligne liée à la timetable par une autre
	 * 
	 * @param line la nouvelle ligne de type Line
	 */
	public void setLine(Line line) {
		this.line = line;
	}

	/**
	 * Renvoie la station liée à la timetable
	 * 
	 * @return une station de type Station
	 */
	public Station getStation() {
		return station;
	}

	/**
	 * Remplace la station liée à la timetable par une autre
	 * 
	 * @param station la nouvelle station de type Station
	 */
	public void setStation(Station station) {
		this.station = station;
	}

	/**
	 * Renvoie la liste de périodes liées à la timetable
	 * 
	 * @return une liste de périodes de type Period
	 */
	public LinkedList<Period> getPeriods() {
		return periods;
	}

	/**
	 * Remplace la liste de périodes liées à la timetable par une autre liste
	 * 
	 * @param periods la nouvelle liste de périodes de type Period
	 */
	public void setPeriods(LinkedList<Period> periods) {
		this.periods = periods;
	}


	/**
	 * Renvoie une unique chaîne de caractères contenant l'ensemble des horaires
	 * pour une période donnée
	 * 
	 * @param pLabel une chaîne de caractères identifiant une période
	 * @return une chaîne de caractères contenant 5 horaires par ligne
	 */
	public String periodsToString(String pLabel) {
		int size = this.periods.size();
		Period period = new Period();
		String toReturn = "";
		String allDates[];

		for (int i = 0; i < size; i++) {
			period = this.periods.get(i);
			if (period.getLabel().equals(pLabel)) {
				allDates = period.getDates();
				int sizeString = allDates.length;
				for (int j = 0; j < sizeString; j++) {
					toReturn += allDates[j] + " ";
					
					//Si il y a déjà 5 horaires sur une ligne, on saute une ligne
					if (j % 5 == 4 && sizeString != j + 1)
						toReturn += "\n";
				}
			}
		}
		return toReturn;
	}
}