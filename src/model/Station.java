package model;

import java.util.ArrayList;

/**
 * La classe <b>Station</b> représente une station avec son numéro, 
 * son nom, sa position avec sa latitude et longitude, le nom de la ville dans
 * laquelle elle est située ainsi que le type de transports qui s'y arrêtent
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Line
 * @see TransportType
 *
 */
public class Station {

	private int number;
	private String name;
	private double latitude;
	private double longitude;
	private String cityName;
	private ArrayList<TransportType> transportTypes;

	/**
	 * Constructeur par défaut<br/>
	 * Initialise une station sans nom ni nom de ville et avec
	 * un numéro, une latitude et une longitude initialisées à 0
	 */
	public Station() {
		super();
		this.number = 0;
		this.name = "";
		this.latitude = 0;
		this.longitude = 0;
		this.cityName = "";
	}

	/**
	 * Crée une station en fonction des données d'une autre station
	 * 
	 * @param temp la station dont les données seront copiées dans la nouvelle station
	 */
	public Station(Station temp) {
		super();
		this.number = temp.number;
		this.name = temp.name;
		this.latitude = temp.latitude;
		this.longitude = temp.longitude;
		this.cityName = temp.cityName;
		this.transportTypes = temp.transportTypes;
	}

	/**
	 * Initialise une station avec les paramètres envoyés
	 * 
	 * @param number un entier correspondant au numéro de la station
	 * @param name une chaîne de caractères correspondant au nom de la station
	 * @param latitude un double correspondant à la latitude de la station
	 * @param longitude un double correspondant à la longitude de la station
	 * @param cityName une chaîne de caractères correspondant au nom de la 
	 * ville dans laquelle est située la station
	 */
	public Station(int number, String name, double latitude, double longitude,
			String cityName) {
		super();
		this.number = number;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.cityName = cityName;
	}

	/**
	 * Renvoie la liste de type de transports passant par cette station
	 * 
	 * @return une liste de types de transports
	 */
	public ArrayList<TransportType> getTransportTypes() {
		return transportTypes;
	}

	/**
	 * Remplace la liste de type de transports par une autre liste
	 * 
	 * @param la nouvelle liste de types de transports
	 */
	public void setTransportTypes(ArrayList<TransportType> transportTypes) {
		this.transportTypes = transportTypes;
	}

	/**
	 * Renvoie le numéro de la station
	 * 
	 * @return un entier correspondant au numéro de la station
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Remplace le numéro de la station par un autre
	 * 
	 * @param number le nouvel entier correspondant au numéro de la station
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * Renvoie le nom de la station
	 * 
	 * @return une chaîne de caractères correspondant au nom de la station
	 */
	public String getName() {
		return name;
	}

	/**
	 * Remplace le nom de la station par un autre
	 * 
	 * @param name la nouvelle de chaîne de caractères correspondant
	 * au nom de la station
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Renvoie la latitude de la station
	 * 
	 * @return un double correspondant à la latitude de la station
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Remplace la latitude de la station par une autre
	 * 
	 * @param latitude le nouveau double correspondant à la 
	 * latitude de la station
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Renvoie la longitude de la station
	 * 
	 * @return un double correspondant à la longitude de la station
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Remplace la longitude de la station par une autre
	 * 
	 * @param longitude le nouveau double correspondant à la 
	 * longitude de la station
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Renvoie le nom de la ville dans laquelle est située la station
	 * 
	 * @return une chaîne de caractères correspondant au nom de la ville
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * Remplace le nom de la ville dans laquelle est située la station
	 * par une autre
	 * 
	 * @param cityName la nouvelle chaîne de caractères correspondant au 
	 * nom de la ville
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
}
