package model;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 * La classe <b>Map</b> représente l'ensemble des stations, des lignes 
 * et des horaires chargés dans le logiciel
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Line
 * @see Station
 * @see Timetable
 *
 */
public class Map {
	private LinkedList<Station> stations;
	private LinkedList<Line> lines;
	private LinkedList<Timetable> timetables;

	/**
	 * Constructeur par défaut<br/>
	 * Initialise une map sans stations, ni lignes, ni horaires
	 */
	public Map() {
		super();
		this.stations = new LinkedList<Station>();
		this.lines = new LinkedList<Line>();
		this.timetables = new LinkedList<Timetable>();
	}

	/**
	 * Renvoie la liste contenant toutes les stations
	 * 
	 * @return la liste de stations
	 */
	public LinkedList<Station> getStations() {
		return stations;
	}

	/**
	 * Remplace la liste contenant toutes les stations par une autre liste
	 * 
	 * @param stations la nouvelle liste de stations
	 */
	public void setStations(LinkedList<Station> stations) {
		this.stations = stations;
	}

	/**
	 * Renvoie la liste contenant toutes les lignes
	 * 
	 * @return la liste de lignes
	 */
	public LinkedList<Line> getLines() {
		return lines;
	}

	/**
	 * Remplace la liste contenant toutes les lignes par une autre liste
	 * 
	 * @param lines la nouvelle liste de lignes
	 */
	public void setLines(LinkedList<Line> lines) {
		this.lines = lines;
	}

	/**
	 * Renvoie la liste contenant toutes les timetables
	 * 
	 * @return la liste de timetables
	 */
	public LinkedList<Timetable> getTimetables() {
		return timetables;
	}

	/**
	 * Remplace la liste contenant toutes les timetables par une autre liste
	 * 
	 * @param timetables la nouvelle liste de timetables
	 */
	public void setTimetables(LinkedList<Timetable> timetables) {
		this.timetables = timetables;
	}

	/**
	 * Renvoie une liste de timetables en fonction d'une ligne et d'une station
	 * 
	 * @param pLine une ligne 
	 * @param pStation une station
	 * @return une liste de timetables 
	 */
	public LinkedList<Timetable> searchSchedule(Line pLine, Station pStation) {
		LinkedList<Timetable> toReturn = new LinkedList<Timetable>();
		int size = this.timetables.size();

		for (int i = 0; i < size; i++) {
			Timetable buffer = this.timetables.get(i);
			if (buffer.getStation() == pStation)
				if (buffer.getLine() == pLine)
					toReturn.add(buffer);
		}
		
		return toReturn;
	}

	/**
	 * Renvoie une liste de timetables en fonction d'une station
	 *  
	 * @param pStation une station
	 * @return une liste de timetables 
	 */
	public LinkedList<Timetable> searchSchedule(Station pStation) {
		LinkedList<Timetable> toReturn = new LinkedList<Timetable>();
		int size = this.timetables.size();

		for (int i = 0; i < size; i++) {
			Timetable buffer = this.timetables.get(i);
			if (buffer.getStation() == pStation)
				toReturn.add(buffer);
		}

		return toReturn;
	}

	/**
	 * Renvoie une liste de lignes contenant la station envoyée en paramètre
	 * 
	 * @param pStation une station
	 * @return une liste de lignes
	 */
	public LinkedList<Line> searchLineOnStations(Station pStation) {
		LinkedList<Line> toReturn = new LinkedList<Line>();
		int size = this.lines.size();

		for (int i = 0; i < size; i++) {

			Line buffer = this.lines.get(i);
			int size2 = buffer.getStations().size();

			for (int j = 0; j < size2; j++) {

				if (buffer.getStations().get(j) == pStation)
					toReturn.add(buffer);
			}
		}
		return toReturn;
	}

	/**
	 * Renvoie une ligne en fonction d'un objet envoyé en paramètre
	 * 
	 * @param pLine un objet récupéré via une liste déroulante
	 * @return une ligne ayant le même nom que l'objet pLine
	 */
	public Line convertToLine(Object pLine) {
		String name = pLine.toString();
		Line toReturn = new Line();

		LinkedList<Line> buffer = this.getLines();
		int size = buffer.size();

		for (int i = 0; i < size; i++) {
			toReturn = buffer.get(i);
			if (toReturn.getName() == name)
				return toReturn;
		}
		return null;
	}

	/**
	 * Renvoie l'heure du prochain passage d'un transport en commun
	 * à une station pour une ligne donnée
	 * 
	 * @param pLine une ligne
	 * @param pStation une station 
	 * @return une chaîne de caractères contenant l'horaire du prochain passage
	 */
	public String getNextArrival(Line pLine, Station pStation) {
		
		//Récupère une liste de timetables liées à une ligne et une station
		LinkedList<Timetable> schedule = this.searchSchedule(pLine, pStation);

		String contenu = "";
		int size = schedule.size();

		//Ajoute tous les horaires dans une chaîne de caractères
		for (int i = 0; i < size; i++) {
			contenu += schedule.get(i).periodsToString("Week-end");
		}
		for (int i = 0; i < size; i++) {
			contenu += schedule.get(i).periodsToString("Semaine");
		}
		
		//S'il n'y a pas d'horaires, retourne null
		if(contenu.trim().isEmpty())
			return null;
		
		Calendar now = Calendar.getInstance();
		
		//Récupère le jour actuel
		int day = now.get(Calendar.DAY_OF_WEEK);
		
		//Récupère l'heure actuelle
		int hour = now.get(Calendar.HOUR_OF_DAY);
		
		//Récupère la minute actuelle
		int minute = now.get(Calendar.MINUTE);

		String label = new String();

		//Récupère le label de la période en fonction du jour actuel
		if (day == Calendar.SUNDAY || day == Calendar.SATURDAY)
			label = "Week-end";
		else
			label = "Semaine";

		return this.nextArrival(pLine, pStation, label, hour, minute);
	}

	/**
	 * Renvoie l'heure et la minute du prochain passage d'un transport en commun
	 * à une station pour une ligne donnée en fonction de la période, l'heure et 
	 * la minute envoyés en paramètres
	 * 
	 * @param pLine une ligne	
	 * @param pStation une station
	 * @param pLabel une chaîne de caractères contenant le label de la période
	 * @param pHour un entier contenant une heure
	 * @param pMinute un entier contenant un nombre de minutes
	 * @return une chaîne de caractères contenant l'heure et la minute du prochain passage
	 */
	private String nextArrival(Line pLine, Station pStation, String pLabel,
			int pHour, int pMinute) {

		LinkedList<Timetable> schedule = this.searchSchedule(pLine, pStation);
		int size = schedule.size();

		int heurepas, minpas;
		String passages = new String();

		//Ajoute dans la chaîne passages les horaires liés à la ligne, station
		//et période envoyés en paramètres
		for (int i = 0; i < size; i++)
			passages += schedule.get(i).periodsToString(pLabel);

		StringTokenizer hash = new StringTokenizer(passages);
		StringTokenizer heuremin;

		//Tant qu'il y a des horaires à vérifier
		while (hash.hasMoreTokens()) {
			//On sépare les éléments grâce aux ":" 
			heuremin = new StringTokenizer(hash.nextToken(), ":");

			//On récupère l'heure puis on passe au prochain token 
			//(après les ":" placés juste après l'heure)
			heurepas = Integer.parseInt(heuremin.nextToken());
			
			//On récupère les minutes
			minpas = Integer.parseInt(heuremin.nextToken());

			//Si un horaire est ultérieur à l'heure envoyée en paramètre
			if (heurepas * 60 + minpas >= pHour * 60 + pMinute) {
				String toReturn = "";

				//si le nombre d'heures de l'horaire est inférieur à 10
				if (heurepas < 10)
					toReturn += "0";
				toReturn += heurepas + ":";
				if (minpas < 10)
					toReturn += "0";
				toReturn += minpas;

				//Sachant que la liste d'horaires est triée, le premier horaire trouvé
				//est forcément l'horaire du prochain passage. On peut donc retourner le résultat
				return toReturn + " - " + pLabel;
			}
		}
		
		//Si le nombre d'heures et de minutes et de 0, cela veut dire qu'aucun
		//horaire a été trouvé pour la période donnée, on change donc de période
		//et on cherche le premier horaire à partir de 0h00
		if (pHour == 0 && pMinute == 0) {
			if (pLabel == "Semaine") {
				return this.nextArrival(pLine, pStation, "Week-end", 0, 0);
			} else if (pLabel == "Week-end") {
				return this.nextArrival(pLine, pStation, "Semaine", 0, 0);
			}
		} else
			return this.nextArrival(pLine, pStation, pLabel, 0, 0);
		
		return null;
	}
}
