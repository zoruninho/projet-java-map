package model;
import java.util.LinkedList;

/**
 * La classe <b>Line</b> représente une ligne de transports en commun
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see TransportType
 * @see Station
 *
 */
public class Line {

	private String name;
	private TransportType transportType;
	private LinkedList<Station> stations;
	
	/**
	 * Constructeur par défaut<br/>
	 * Initialise une ligne sans nom ni stations
	 */
	public Line() {
		super();
		this.name="";
		this.transportType=TransportType.BUS;
		this.stations=new LinkedList<Station>();
	}
	
	/**
	 * Crée une ligne en fonction des données d'une autre ligne
	 * 
	 * @param temp la ligne dont les données seront copiées dans la nouvelle ligne
	 */
	public Line(Line temp) {
		super();
		this.name = temp.name;
		this.transportType = temp.transportType;
		this.stations = temp.stations;
	}
	
	/**
	 * Initialise une ligne avec les paramètres envoyés
	 * 
	 * @param name une chaîne de caractères identifiant la ligne
	 * @param transportType un type de transport associé à la ligne
	 * @param stations une liste de stations de type Station
	 */
	public Line(String name, TransportType transportType,
			LinkedList<Station> stations) {
		super();
		this.name = name;
		this.transportType = transportType;
		this.stations = stations;
	}

	/**
	 * Renvoie le nom de la ligne
	 * 
	 * @return une chaîne de caractères identifiant la ligne
	 */
	public String getName() {
		return name;
	}

	/**
	 * Remplace le nom de la ligne par un autre
	 * 
	 * @param name le nouveau nom de la ligne
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Renvoie le type de transport de la ligne
	 * 
	 * @return le type de transport (Bus, métro ou tramway) de la ligne
	 */
	public TransportType getTransportType() {
		return transportType;
	}

	/**
	 * Remplace le type de transport de la ligne par un autre
	 * 
	 * @param transportType le nouveau type de transport de la ligne
	 */
	public void setTransportType(TransportType transportType) {
		this.transportType = transportType;
	}
	
	/**
	 * Renvoie la liste de stations de la ligne
	 * 
	 * @return une liste de stations 
	 */
	public LinkedList<Station> getStations() {
		return stations;
	}

	/**
	 * Remplace la liste de stations de la ligne par une autre liste de stations
	 * 
	 * @param stations la nouvelle liste de stations
	 */
	public void setStations(LinkedList<Station> stations) {
		this.stations = stations;
	}
	
	/**
	 * Renvoie les noms des stations de la ligne
	 * 
	 * @return un tableau de chaînes de caractères contenant les noms des stations
	 */
	public String[] getStationsNames()
	{
		String[] tempTab = new String[this.getStations().size()];
		
		for(int i=0; i<this.getStations().size(); i++)
		{
			tempTab[i]=this.stations.get(i).getName();
		}
		
		return tempTab;
	}
	
	/**
	 * Renvoie la position d'une station dans la liste de stations de la ligne
	 * 
	 * @param stationName le nom de la station dont la position est à trouver
	 * @return un entier contenant la position de la station dans la liste de stations de la ligne
	 */
	public int getPositionStation(String stationName)
	{
		String[] tempTab = this.getStationsNames();
		
		for(int i=0;i<tempTab.length;i++)
		{
			if(tempTab[i]==stationName)
				return i+1;
		}
		
		return 0;
	}
	
	/**
	 * Renvoie les noms des stations de la ligne, sauf celui de la dernière station
	 * 
	 * @return un tableau de chaînes de caractères contenant les noms des stations
	 */
	public String[] getStationsNamesExceptLast()
	{
		String[] tempTab = new String[this.getStations().size()-1];
		
		for(int i=0; i<this.getStations().size()-1; i++)
		{
			tempTab[i]=this.stations.get(i).getName();
		}
		
		return tempTab;
	}
}
