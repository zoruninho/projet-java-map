package controller;


import model.Period;
import model.Timetable;
import view.MainFrame;

/**
 * La classe principale <b>Main</b> du programme
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 *
 */
public class Main {
	
	/**
	 * Crée un nouveau controlleur ainsi qu'une fenêtre principale
	 * 
	 * @param args les arguments passés au programme lors du lancement
	 */
	public static void main(String[] args) {
	
		Controller controller = new Controller();
		new MainFrame(controller);
	}
}