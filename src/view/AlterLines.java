package view;

import java.awt.Choice;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import controller.Controller;
import model.Line;
import model.Period;
import model.Station;
import model.Timetable;
import model.TransportType;

/**
 * La classe <b>AlterLines</b> hérite de la classe abstraite AlterDatas
 * et permet de modifier les informations d'une ligne
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Line
 * 
 */
public class AlterLines extends AlterDatas{

	Line temp;
	Line temp2;
	int positionInList;
	
	/**
	 * Initialise une AlterLines avec deux lignes vides
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public AlterLines(Controller controller) {
		super();
		this.temp = new Line();
		this.temp2 = new Line();
		this.controller = controller;
	}

	/**
	 * Lance une pop-up permettant de choisir la ligne à modifier
	 */
	@Override
	public void choicePopup() {
		String[] lines = controller.getLinesNames();
		
		//Crée une liste déroulante avec système d'autocomplétion contenant
		//le nom des lignes
		JComboBox line = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        line, GlazedLists.eventListOf(lines));
		
		//Crée un message contenant une chaîne de caractères et la liste déroulante
		Object[] message = new Object[] { "Ligne : ", line};
		
		//Crée la pop-up contenant le message 
		int r = JOptionPane.showConfirmDialog(null, message,
				"Choisir la ligne à modifier", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			temp.setName(line.getSelectedItem().toString());
			temp2.setName(line.getSelectedItem().toString());
			alterPopup();
		}
	}

	/**
	 * Lance la pop-up permettant de modifier les informations de la ligne sélectionnée
	 */
	@Override
	public void alterPopup() {
		Station station1 = new Station();
		Station station2 = new Station();
		
		//Vérifie que la ligne existe bien
		temp=controller.copyLine(temp);
		if(temp==null)
		{
			errorPopup("noLine");
			return;
		}
		temp=new Line(temp);
		
		//Récupère la position de la ligne dans la liste de lignes
		positionInList=controller.positionInListLine(temp);
		
		//Champ de texte modifiable contenant le nom de la ligne
		JTextField name = new JTextField(temp.getName());
		
		//Liste déroulante contenant les types de transports possibles
		Choice transportType = new Choice();
		transportType.addItem(TransportType.BUS.toString());
		transportType.addItem(TransportType.METRO.toString());
		transportType.addItem(TransportType.TRAMWAY.toString());
		transportType.select(temp.getTransportType().toString());
		
		String[] stations = controller.getStationsNames();
		
		//Liste déroulante avec système d'auto-complétion contenant les
		//noms des stations
		JComboBox terminus1 = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        terminus1, GlazedLists.eventListOf(stations));
		terminus1.setSelectedItem(temp.getStations().get(0).getName());
		
		JComboBox terminus2 = new JComboBox();
		AutoCompleteSupport support2 = AutoCompleteSupport.install(
		        terminus2, GlazedLists.eventListOf(stations));
		terminus2.setSelectedItem(temp.getStations().get(1).getName());
		
		//Message contenant le champ de texte et les 3 listes déroulantes
		Object[] message = new Object[] { name, transportType, terminus1, terminus2};
		
		//Lance la pop-up de modification contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Modifiez les valeurs", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			String tempType = transportType.getSelectedItem();
			if(tempType=="BUS")
			temp.setTransportType(TransportType.BUS);
			else if(tempType=="METRO")
			temp.setTransportType(TransportType.METRO);
			else if(tempType=="TRAMWAY")
			temp.setTransportType(TransportType.TRAMWAY);
			
			//On remplace les informations avec les nouvelles
			temp.setName(name.getText());
			station1.setName(terminus1.getSelectedItem().toString());
			station2.setName(terminus2.getSelectedItem().toString());
			station1=controller.copyStation(station1);
			station2=controller.copyStation(station2);
			
			//On vérifie que les nouvelles stations sélectionnées
			//existent bien
			if(station1!=null && station2!=null)
			{
			temp.getStations().set(0, station1);
			temp.getStations().set(temp.getStations().size()-1, station2);
			
			confirmPopup();
			}
			else 
				errorPopup("stationNull");
		}
	}

	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "noLine" :
			JOptionPane.showMessageDialog(null,"Erreur, ligne non existante");
		break;
		case "stationNull" :
			JOptionPane.showMessageDialog(null,"Erreur, au moins une station n'existe pas");
		break;
		}
	}
	
	/**
	 * Lance la pop-up permettant de confirmer la modification des données
	 */
	@Override
	public void confirmPopup() {
		temp2=controller.copyLine(temp2);
		
		//Message contenant des chaînes de caractères et les nouvelles données de la ligne
		Object[] message = new Object[] { "Nom de la ligne : "+temp.getName(), "Type de transport : "+temp.getTransportType(),
				"Station de départ : "+temp.getStations().get(0).getName(), "Station d'arrivée : "+temp.getStations().get(1).getName()
				};
		
		//On change le nom du bouton "OK" par "Valider"
		UIManager.put("OptionPane.okButtonText", "Valider");  
		
		//Lance la pop-up de confirmation
		int r = JOptionPane.showConfirmDialog(null, message,
				"Vérifiez vos informations", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			temp2.setName(temp.getName());
			temp2.setTransportType(temp.getTransportType());
			
			temp2.getStations().set(0, temp.getStations().get(0));
			temp2.getStations().set(temp2.getStations().size()-1, temp.getStations().get(temp.getStations().size()-1));
			
			//Modification de la ligne
			controller.alterLine(temp2,positionInList);
		}
	}
}
