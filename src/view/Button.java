package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import controller.Controller;

/**
 * La classe <b>Button</b> hérite de la classe JButton
 * et implémente ActionListener. Elle permet d'avoir un bouton
 * personnalisé
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 */
public class Button extends JButton implements ActionListener {

	private static final long serialVersionUID = 1L;
	Controller controller;

	/**
	 * Initialise un bouton à la position (0,0) et un nom envoyé
	 * en paramètre
	 * 
	 * @param pName une chaîne de caractères identifiant le bouton
	 */
	public Button(String pName) {
		super(pName);
		this.setLocation(0, 0);
		this.addActionListener(this);
	}

	/**
	 * Initialise un bouton placé à un endroit avec une taille précise et un
	 * nom envoyé en paramètre
	 * 
	 * @param pName une chaîne de caractères identifiant le bouton
	 * @param posX un entier correspondant à la position X du bouton
	 * @param posY un entier correspondant à la position Y du bouton
	 * @param sizeX un entier correspondant à la largeur du bouton
	 * @param sizeY un entier correspondant à la hauteur du bouton
	 */
	public Button(String pName, int posX, int posY, int sizeX, int sizeY) {
		super(pName);
		this.setBounds(posX, posY, sizeX, sizeY);
		this.addActionListener(this);
	}

	/**
	 * Initialise un bouton placé à un endroit avec une taille précise et un
	 * nom envoyé en paramètre ainsi qu'une couleur
	 * 
	 * @param pName une chaîne de caractères identifiant le bouton
	 * @param posX un entier correspondant à la position X du bouton
	 * @param posY un entier correspondant à la position Y du bouton
	 * @param sizeX un entier correspondant à la largeur du bouton
	 * @param sizeY un entier correspondant à la hauteur du bouton
	 * @param pColor une couleur de type Color
	 */
	public Button(String pName, int posX, int posY, int sizeX, int sizeY,
			Color pColor) {
		super(pName);
		this.setBounds(posX, posY, sizeX, sizeY);
		this.setBackground(pColor);
		this.addActionListener(this);
	}

	/**
	 * Initialise un bouton placé à un endroit avec une taille précise et un
	 * nom envoyé en paramètre ainsi qu'une couleur et le controlleur
	 * 
	 * @param pName une chaîne de caractères identifiant le bouton
	 * @param posX un entier correspondant à la position X du bouton
	 * @param posY un entier correspondant à la position Y du bouton
	 * @param sizeX un entier correspondant à la largeur du bouton
	 * @param sizeY un entier correspondant à la hauteur du bouton
	 * @param pColor une couleur de type Color
	 * @param pController le controlleur principal du programme
	 */
	public Button(String pName, int posX, int posY, int sizeX, int sizeY,
			Color pColor, Controller pController) {
		super();
	
		this.setBounds(posX, posY, sizeX, sizeY);
		this.setBackground(pColor);
		this.addActionListener(this);
		this.setActionCommand(pName);

		this.setBorderPainted(false);
		this.setRolloverEnabled(true);

		this.controller = pController;

		this.setToolTipText(pName);
	}
	
	/**
	 * Initialise un bouton avec le nom envoyé en paramètre ainsi qu'une image
	 * 
	 * @param pName une chaîne de caractères identifiant le bouton
	 * @param pImageIcon une image 
	 */
	public Button(String pName, ImageIcon pImageIcon) {
		super(pImageIcon);
		this.addActionListener(this);
		this.setActionCommand(pName);
	}

	/**
	 * Initialise un bouton avec le nom envoyé en paramètre ainsi qu'une image
	 * grâce à son chemin d'accès
	 * 
	 * @param pName une chaîne de caractères identifiant le bouton
	 * @param pImagePath une chaîne de caractères contenant le chemin d'accès
	 */
	public Button(String pName, String pImagePath) {
		ImageIcon img = new ImageIcon(pImagePath);
		this.setIcon(img);
		this.addActionListener(this);
		this.setActionCommand(pName);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
}
