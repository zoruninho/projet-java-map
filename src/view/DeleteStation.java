package view;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import model.Station;
import model.Timetable;
import controller.Controller;

/**
 * La classe <b>DeleteStation</b> hérite de la classe abstraite DeleteDatas
 * et permet de supprimer une station
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Station
 * 
 */
public class DeleteStation extends DeleteDatas{

	Station temp;
	int positionInList;

	/**
	 * Initialise une DeleteStation avec une station vide
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public DeleteStation(Controller controller) {
		super();
		this.temp = new Station();
		this.controller = controller;
	}
	
	/**
	 * Lance une pop-up permettant de choisir la ligne à supprimer
	 */
	@Override
	public void choicePopup() {
		String[] stations = controller.getStationsNames();
		
		//Crée une liste déroulante avec système d'autocomplétion contenant
		//les noms des stations
		JComboBox station = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        station, GlazedLists.eventListOf(stations));
		
		//Crée un message contenant une chaîne de caractères et la liste déroulante
		Object[] message = new Object[] { "Station : ", station};
		
		//Crée la pop-up contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Choisir la station à modifier", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			temp.setName(station.getSelectedItem().toString());
			confirmPopup();
		}
	}
	
	/**
	 * Lorsque la suppression est lancée depuis un clic droit, la station 
	 * temporaire devient la station concernée par le clic droit
	 * 
	 * @param station la station concernée par le clic droit
	 */
	public void setStation(Station station)
	{
		this.temp = station;
	}

	/**
	 * Lance la pop-up permettant de confirmer la suppression des données
	 */
	@Override
	public void confirmPopup() {
		temp=controller.copyStation(temp);
		if(temp==null)
		{
			errorPopup("noStation");
			return;
		}
		
		//Récupère la position de la station dans la liste de stations
		positionInList=controller.positionInListStation(temp);
		
		//Message contenant des chaînes de caractères et le nom de la station à supprimer
		Object[] message = new Object[] { "Voulez-vous vraiment supprimer la station "+temp.getName()+" ?\n",
				"Attention, des répercussions peuvent avoir lieu sur certaines lignes"};
		
		//Lance la pop-up de suppression contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Suppression", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			//On supprime la station 
			controller.deleteStation(positionInList, temp);
		}
	}
	
	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "noStation" :
			JOptionPane.showMessageDialog(null,"Erreur, station non existante");
		break;
		}
	}
}
