package view;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.Controller;
import model.Station;

/**
 * La classe abstraite <b>DeleteDatas</b> contient les informations et les méthodes
 * qui définissent une pop-up permettant de supprimer des données 
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 */
public abstract class DeleteDatas extends JFrame{
	
	protected String boxName;
	protected JDialog dialogBox;
	protected JPanel panel;
	protected int width;
	protected int height;
	protected Controller controller;
	
	public abstract void choicePopup();
	public abstract void confirmPopup();
}
