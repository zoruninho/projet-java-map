package view;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.Controller;
import model.Line;
import model.Period;
import model.Station;

/**
 * La classe abstraite <b>AlterDatas</b> contient les informations et les méthodes
 * qui définissent une pop-up permettant de modifier des données 
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 */
public abstract class AlterDatas extends JFrame{
	
	protected String boxName;
	protected JDialog dialogBox;
	protected JPanel panel;
	protected int width;
	protected int height;
	protected Controller controller;
	
	public abstract void choicePopup();
	public abstract void alterPopup();
	public abstract void confirmPopup();
	public void confirmPopup(Station station1, Station station2){};
}
