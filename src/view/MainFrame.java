package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.Controller;

/**
 * La classe <b>MainFrame</b> hérite de JFrame et permet de créer la fenêtre
 * principale ainsi que la toolbar qui est y incluse
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 *
 */
public class MainFrame extends JFrame {
	
	private Controller controller;
	
	/**
	 * Initialise une MainFrame, lui donne un nom, une taille, une icone, 
	 * une barre de menu et une barre d'outils
	 * 
	 * @param controller le controlleur principal de l'application
	 */
	public MainFrame(Controller controller){
		this.controller = controller;
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setTitle("Java Map Builder");
		
		//On peut redimensionner la fenêtre
		this.setResizable(true);
		
		this.setSize(800, 600);
		this.setMinimumSize(new Dimension(640, 480));
		
		//On centre la fenêtre
		this.setLocationRelativeTo(null);
		
		this.setIconImage(Toolkit
				.getDefaultToolkit().getImage("picture/test.png"));
		
		//Nouvelle barre de menu
		MenuBar mb = new MenuBar(controller);
		
		this.setJMenuBar(mb);
		
		JPanel fond = new JPanel();
		
		PersonnalPanel pan = new PersonnalPanel(controller);
		
		pan.setBackground(new Color(240,240,240));
		
		fond.setLayout(new BorderLayout());		

		//Nouvelle barre d'outils
		ToolBar tb = new ToolBar();
		tb.setFloatable(false);
		
		this.addButtonToolbar(tb, pan);
		
		fond.add(tb, BorderLayout.NORTH);
		
		fond.add(pan, BorderLayout.CENTER);
		
		this.setContentPane(fond);
	    
	    this.setVisible(true);
	}
	
	/**
	 * Ajoute des éléments à la barre d'outils, chaque élément ayant un nom,
	 * une image, et un lien vers le controlleur
	 * 
	 * @param tb la barre d'outils sur laquelle implémenter les boutons
	 * @param pan le panel sur lequel est la barre d'outils
	 */
	public void addButtonToolbar(ToolBar tb, PersonnalPanel pan){
		
		tb.add(new ToolBarButton("new", "picture/new.png", controller));
		
		tb.add(new ToolBarButton("open", "picture/open.png", controller));
	
		tb.add(new ToolBarButton("save", "picture/save.png", controller));
		
		tb.add(new ToolBarButton("S+", "picture/station.png", controller));
		
		tb.add(new ToolBarButton("L+", "picture/line.png", controller));

		tb.add(new ToolBarButton("H+", "picture/timetable.png", controller));
		
		tb.add(new ToolBarButton("zoom0", "picture/zoom0.png", controller, pan));
		
		tb.add(new ToolBarButton("zoomIn", "picture/zoomIn.png", controller, pan));
		
		tb.add(new ToolBarButton("zoomOut", "picture/zoomOut.png", controller, pan));
		
		tb.add(new ToolBarButton("activatemap", "picture/activatemap.png", controller, pan));
		
		tb.add(new ToolBarButton("deactivatemap", "picture/deactivatemap.png", controller, pan));
		
		tb.add(new ToolBarButton("activatesat", "picture/activatesat.png", controller, pan));
	}
}
