
package view;

import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import controller.Controller;

/**
 * La classe <b>ToolBarButton</b> hérite de JButton et implémente
 * ActionListener. Elle permet de créer des boutons plus spécifiques 
 * pour une barre d'outils
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 *
 */
public class ToolBarButton extends JButton implements ActionListener {

	private static final long serialVersionUID = 1L;
	Controller controller;
	PersonnalPanel pan;

	/**
	 * Initialise un ToolBarButton avec un nom
	 * 
	 * @param pName une chaîne de caractères correspondant au nom du bouton
	 * @param controller le controlleur par défaut de l'application
	 */
	public ToolBarButton(String pName, Controller controller) {
		super(pName);
		this.setLocation(0, 0);
		this.controller = controller;
		this.addActionListener(this);
	}

	/**
	 * Initialise un ToolBarButton avec un nom et une image
	 *  
	 * @param pName une chaîne de caractères correspondant au nom du bouton
	 * @param pImagePath une chaîne de caractères correspondant au chemin d'accès à l'image
	 * @param controller le controlleur par défaut de l'application
	 */
	public ToolBarButton(String pName, String pImagePath, Controller controller) {
		ImageIcon img = new ImageIcon(pImagePath);
		this.setIcon(img);
		this.controller = controller;
		this.addActionListener(this);
		this.setActionCommand(pName);
	}
	
	/**
	 * Initialise un ToolBarButton avec un nom et une image, et un PersonnalPanel
	 * 
	 * @param pName pName une chaîne de caractères correspondant au nom du bouton
	 * @param pImagePath une chaîne de caractères correspondant au chemin d'accès à l'image
	 * @param controller le controlleur par défaut de l'application
	 * @param pan le PersonnalPanel du ToolBarButton
	 */
	public ToolBarButton(String pName, String pImagePath, Controller controller, PersonnalPanel pan) {
		 ImageIcon img = new ImageIcon(pImagePath);
		// ImageIcon img = new ImageIcon(ClassLoader.getSystemResource(pImagePath));
		
		
		this.setIcon(img);
		this.controller = controller;
		this.addActionListener(this);
		this.setActionCommand(pName);
		this.pan=pan;
	}
	
	/**
	 * Permet de gérer les interactions avec les ToolBarButtons
	 */
	public void actionPerformed(ActionEvent e) {
		String name = e.getActionCommand();
		
		switch (name)
		{
			case "S+" :
				InsertStations insertStations = new InsertStations(controller);
				insertStations.createPopup();
				break;
			case "save" :
				controller.save();
				break;
			case "open" :
				FileDialog fileDialog = new FileDialog(new JFrame(), "Charger", FileDialog.LOAD);
				fileDialog.show();
				
				//Si on a sélectionné un fichier
				if (fileDialog.getFile() != null && fileDialog.getDirectory() != null)
				{
					String fileName = fileDialog.getDirectory() + fileDialog.getFile();
					
					controller.setFileName(fileName);
				}	
				break;
			case "new" :
				fileDialog = new FileDialog(new JFrame(), "Nouveau", FileDialog.SAVE);
				fileDialog.show();
				
				if (fileDialog.getFile() != null && fileDialog.getDirectory() != null)
				{
					controller.createEmptyJson(fileDialog.getDirectory() + fileDialog.getFile());
				}
				break;
			case "L+" :
				InsertLines insertLines = new InsertLines(controller);
				insertLines.createPopup();
				break;
			case "H+" :
				InsertTimetable insertTimetable = new InsertTimetable(controller);
				insertTimetable.createPopup();
				break;
			case "zoom0" :
				pan.mouseWheelValue=0;
				pan.xDeb=pan.maxMin[0][0];
				pan.yDeb=pan.maxMin[0][1];
				pan.xPos=pan.xMid;
				pan.yPos=pan.yMid;
				pan.xDec=0;
				pan.yDec=0;
				pan.toRecalculate=true;
				controller.repaintAllWindows();
				break;
			case "zoomIn" :
				pan.mouseWheelValue+=0.1;
				pan.toRecalculate=true;
				controller.repaintAllWindows();
				break;
			case "zoomOut" :
				pan.mouseWheelValue-=0.1;
				pan.toRecalculate=true;
				controller.repaintAllWindows();
				break;
			case "activatemap" : 
				if(!pan.mapViewEnabled)
				{
					pan.mapType="map";
					pan.mapViewEnabled=true;
					controller.repaintAllWindows();
				}
				else
				{
					pan.mapType="map";
					controller.repaintAllWindows();
				}
				break;
			case "deactivatemap" : 
				if(pan.mapViewEnabled)
				{
					pan.mapViewEnabled=false;
					controller.repaintAllWindows();
				}
				break;
			case "activatesat" : 
				if(pan.mapViewEnabled)
				{
					pan.mapType="hyb";
					controller.repaintAllWindows();
				}
				else
				{
					pan.mapViewEnabled=true;
					pan.mapType="hyb";
					controller.repaintAllWindows();
				}
				break;
		}
	}
}
