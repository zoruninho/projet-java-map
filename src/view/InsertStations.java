package view;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import model.Station;
import controller.Controller;

/**
 * La classe <b>InsertStations</b> hérite de la classe abstraite InsertDatas
 * et permet d'ajouter une station
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Station
 * 
 */
public class InsertStations extends InsertDatas {

	Station temp;

	/**
	 * Initialise une InsertStations avec une station vide
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public InsertStations(Controller controller) {
		super();
		this.temp = new Station();
		this.controller = controller;
	}
	
	/**
	 * Ajoute les coordonnées de la nouvelle station
	 * 
	 * @param longitude un double correspondant à la longitude de la station
	 * @param latitude un double correspondant à la latitude de la station
	 */
	public void setCoordinate(double longitude, double latitude)
	{
		temp.setLongitude(longitude);
		temp.setLatitude(latitude);
	}
	
	/**
	 * Lance une pop-up permettant de créer une nouvelle station
	 */
	public void createPopup() {

		//Champs de texte vides
		JTextField name = new JTextField();
		JTextField cityName = new JTextField();
		
		//Crée un message contenant des chaînes de caractères ainsi que les champs de texte
		Object[] message = new Object[] { "Nom de la station : ", name,
				"Ville : ", cityName};
		
		//Crée la pop-up contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Ajouter une station", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			if(name.getText().trim().isEmpty() || cityName.getText().trim().isEmpty())
			{
				errorPopup("names");
				return;
			}
			temp.setName(name.getText());
			temp.setCityName(cityName.getText());
			temp.setNumber(controller.getMaxStationNumber()+1);
			
			//Permet de placer une station en cliquant sur la carte
			this.controller.setStationToPlace(this);
		}
	}

	/**
	 * Lance la pop-up permettant de confirmer l'insertion des données
	 */
	@Override
	public void confirmPopup() {
		
		//Message contenant des chaînes de caractères et les données de la station
		Object[] message = new Object[] {
				"Nom de la station : " + temp.getName(),
				"Ville : " + temp.getCityName(),
				"Longitude : "+temp.getLongitude(),
				"Latitude : "+temp.getLatitude() };
		
		//On change le nom du bouton "OK" par "Valider"
		UIManager.put("OptionPane.okButtonText", "Valider");
		
		//Lance la pop-up de confirmation
		int r = JOptionPane.showConfirmDialog(null, message,
				"Vérifiz vos informations", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			//Insère la station
			controller.insertFilledStation(temp);
		}
	}
	
	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "names" :
			JOptionPane.showMessageDialog(null,"Erreur, il faut entrer un nom de station et de ville");
		break;
		}
	}

}
