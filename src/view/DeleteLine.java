package view;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import model.Line;
import model.Timetable;
import controller.Controller;

/**
 * La classe <b>DeleteLine</b> hérite de la classe abstraite DeleteDatas
 * et permet de supprimer une ligne
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Line
 * 
 */
public class DeleteLine extends DeleteDatas{

	Line temp;
	int positionInList;
	
	/**
	 * Initialise une DeleteLine avec une ligne vide
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public DeleteLine(Controller controller) {
		super();
		this.temp = new Line();
		this.controller = controller;
	}

	/**
	 * Lance une pop-up permettant de choisir la ligne à supprimer
	 */
	@Override
	public void choicePopup() {
		String[] lines = controller.getLinesNames();
		
		//Crée une liste déroulante avec système d'autocomplétion contenant
		//les noms des lignes
		JComboBox line = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        line, GlazedLists.eventListOf(lines));
		
		//Crée un message contenant une chaîne de caractères et la liste déroulante
		Object[] message = new Object[] { "Ligne : ", line};
		
		//Crée la pop-up contenant le message 
		int r = JOptionPane.showConfirmDialog(null, message,
				"Choisir la ligne à supprimer", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			temp.setName(line.getSelectedItem().toString());
			confirmPopup();
		}
	}

	/**
	 * Lance la pop-up permettant de confirmer la suppression des données
	 */
	@Override
	public void confirmPopup() {
		temp=controller.copyLine(temp);
		if(temp==null)
		{
			errorPopup("noLine");
			return;
		}
		
		//Récupère la position de la ligne dans la liste de lignes
		positionInList=controller.positionInListLine(temp);
		
		//Message contenant des chaînes de caractères et le nom de la ligne à supprimer
		Object[] message = new Object[] { "Voulez-vous vraiment supprimer la ligne "+temp.getName()+" ?\n",
				"Attention, les horaires liés à cette ligne seront également supprimés"};
		
		//Lance la pop-up de suppression contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Suppression", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			//On supprime la ligne et les horaires liés à celle-ci
			controller.deleteLineAndTimetables(positionInList, temp.getName());
		}
	}
	
	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "noLine" :
			JOptionPane.showMessageDialog(null,"Erreur, ligne non existante");
		break;
		}
	}
}
