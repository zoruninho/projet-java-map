package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JToolBar;

/**
 * La classe <b>Toolbar</b> hérite de JToolBar et implémente
 * ActionListener. Elle permet de créer une barre d'outils
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 *
 */
public class ToolBar extends JToolBar implements ActionListener {
	
	/**
	 * Constructeur par défaut<br/>
	 * Initialise une barre d'outils vide
	 */
	public ToolBar(){
		super();
	}
	
	/**
	 * Initialise une barre d'outils avec un bouton
	 * 
	 * @param pButton un bouton personnalisé
	 */
	public ToolBar(Button pButton){
		super();
		this.add(pButton);
	}
	
	/**
	 * Initialise une barre d'outils avec de multiples boutons
	 * 
	 * @param pButton un tableau de boutons personnalisés
	 */
	public ToolBar(Button[] pButton){
		super();
		this.addButtons(pButton);
	}
	
	/**
	 * Ajoute des boutons à la barre d'outils
	 * 
	 * @param pButton un tableau de boutons personnalisés
	 */
	public void addButtons(Button[] pButton){
		int max = pButton.length;
		for(int i = 0; i < max; i++){
			this.add(pButton[i]);
		}
	}

	public void actionPerformed(ActionEvent e) {}

}
