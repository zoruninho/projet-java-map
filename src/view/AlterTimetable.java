package view;

import java.awt.Choice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.UIManager;

import model.Line;
import model.Period;
import model.Station;
import model.Timetable;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import controller.Controller;

/**
 * La classe <b>AlterTimetable</b> hérite de la classe abstraite AlterDatas
 * et permet de modifier les informations d'une timetable
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Timetable
 * 
 */
public class AlterTimetable extends AlterDatas implements ActionListener {
	
	JComboBox line;
	JComboBox station;
	Timetable temp;
	Timetable temp2;
	int positionInList;
	int numberPeriod;
	
	/**
	 * Initialise une AlterTimetable avec deux timetables vides
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public AlterTimetable(Controller controller) {
		super();
		this.temp = new Timetable();
		this.temp2 = new Timetable();
		this.controller = controller;
	}
	
	/**
	 * Initialise une AlterTimetable avec une timetable vide et une
	 * autre timetable envoyée en paramètre
	 * 
	 * @param controller le controlleur principal du logiciel
	 * @param temp une timetable existante
	 */
	public AlterTimetable(Controller controller, Timetable temp) {
		super();
		this.temp = temp;
		this.temp2 = new Timetable();
		this.controller = controller;
	}

	/**
	 * Lance une pop-up permettant de choisir la ligne à modifier
	 */
	@Override
	public void choicePopup() {
		String[] lines = controller.getLinesNames();
		
		//Crée une liste déroulante avec système d'autocomplétion contenant
		//le nom des lignes
		line = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        line, GlazedLists.eventListOf(lines));
		
		//Ajoute un listener sur la liste déroulante qui permet d'actualiser
		//la liste déroulante "station" en fonction du choix de ligne
		line.addActionListener(this);
		
		//Crée une liste déroulante vierge
		station = new JComboBox();
		
		//Liste déroulante contenant "semaine" ou "week-end"
		Choice period = new Choice();
		period.addItem("Semaine");
		period.addItem("Week-end");
		
		Line tempLine = new Line();
		Station tempStation = new Station();
		Period tempPeriod = new Period();
		
		//Crée un message contenant des chaînes de caractères et les listes déroulantes
		Object[] message = new Object[] { "Ligne : ", line,
				"Station : ", station, "Période : " , period};
		
		//Crée la pop-up contenant le message 
		int r = JOptionPane.showConfirmDialog(null, message,
				"Modifier des horaires", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			tempLine.setName(line.getSelectedItem().toString());
			tempStation.setName(station.getSelectedItem().toString());
			
			if(period.getSelectedItem()=="Semaine")
				numberPeriod=0;
			else
				numberPeriod=1;
			
			temp.setLine(tempLine);
			temp.setStation(tempStation);
			
			alterPopup();
		}
	}

	/**
	 * Lance la pop-up permettant de modifier les informations de la ligne sélectionnée
	 */
	@Override
	public void alterPopup() {
		temp=controller.copyTimetable(temp);
		
		//Vérifie que la timetable existe bien
		if(temp==null)
		{
			errorPopup("noTimetable");
			return;
		}
		temp=new Timetable(temp);
		
		//Récupère la position de la timetable dans la liste de timetables
		positionInList=controller.positionInListTimetable(temp);
		int numberOfPasses = temp.getPeriods().get(numberPeriod).getPasses().size();
		
		//Crée des champs pour modifier les horaires
		JSpinner time[] = new JSpinner[numberOfPasses]; 
		for(int i=0;i<numberOfPasses;i++)
		{
			time[i] = new JSpinner(new SpinnerDateModel());
			JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(time[i], "HH:mm");
			time[i].setEditor(timeEditor);
			time[i].setValue(temp.getPeriods().get(numberPeriod).getPasses().get(i));
		}
		
		//Message contenant les champs de modification d'horaire
		Object[] message = new Object[] { "Horaires : ", time};
		
		//On change le nom du bouton "YES" par "Rajouter des horaires"
		UIManager.put("OptionPane.yesButtonText", "Rajouter des horaires");  
		
		//On change le nom du bouton "NO" par "Finaliser"
		UIManager.put("OptionPane.noButtonText", "Finaliser");
		
		//Lance la pop-up de modification contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Modifier des horaires", JOptionPane.YES_NO_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Rajouter des horaires" ou "Finaliser"
		if (r == JOptionPane.YES_OPTION || r == JOptionPane.NO_OPTION)
		{
			//Si l'utilisateur a cliqué sur le bouton "rajouter des horaires"
			if(r == JOptionPane.YES_OPTION)
			{
				temp.getPeriods().get(numberPeriod).getPasses().clear();
				for(int i=0;i<numberOfPasses;i++)
					temp.getPeriods().get(numberPeriod).getPasses().add((Date)time[i].getValue());
				
				insertTimetables();
			}
			else
			{
				temp.getPeriods().get(numberPeriod).getPasses().clear();
				for(int i=0;i<numberOfPasses;i++)
					temp.getPeriods().get(numberPeriod).getPasses().add((Date)time[i].getValue());
				
				confirmPopup();
			}
		}
	}

	/**
	 * Lance une pop-up permettant de rajouter un horaire
	 */
	public void insertTimetables(){
		Calendar cal = Calendar.getInstance();
		cal.set(1970,0,1,8,0,0);
		JSpinner time = new JSpinner( new SpinnerDateModel() );
		JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(time, "HH:mm");
		time.setEditor(timeEditor);
		time.setValue(cal.getTime());
		
		Object[] message = new Object[] { "Horaire : ", time};
		
		UIManager.put("OptionPane.yesButtonText", "Valider et continuer");  
		UIManager.put("OptionPane.noButtonText", "Finaliser");  
		int r = JOptionPane.showConfirmDialog(null, message,
				"Ajouter des horaires", JOptionPane.YES_NO_CANCEL_OPTION);
		if (r == JOptionPane.YES_OPTION || r == JOptionPane.NO_OPTION)
		{
			if(r == JOptionPane.YES_OPTION)
			{
				temp.getPeriods().get(numberPeriod).getPasses().add((Date)time.getValue());
				insertTimetables();
			}
			else
			{
				Collections.sort(temp.getPeriods().get(numberPeriod).getPasses());
				confirmPopup();
			}
		}
	}
	
	/**
	 * Lance la pop-up permettant de confirmer la modification des données
	 */
	@Override
	public void confirmPopup() {
		temp2=controller.copyTimetable(temp2);
		String[] dates = temp.getPeriods().get(numberPeriod).getDates();
		String horaires = new String();
		
		horaires+="Horaires : \n";
		for(int i=0;i<dates.length;i++)
		{
			horaires+=dates[i];
			horaires+="\n";
		}
		
		//Message contenant des chaînes de caractères et les nouvelles données de la station
		Object[] message = new Object[] { "Nom de la ligne : "+temp.getLine().getName(),
				"Nom de la station : "+temp.getStation().getName(),
				"Période : "+temp.getPeriods().get(numberPeriod).getLabel(),
				horaires
				};
		
		//On change le nom du bouton "OK" par "Valider"
		UIManager.put("OptionPane.okButtonText", "Valider");  
		
		//Lance la pop-up de confirmation
		int r = JOptionPane.showConfirmDialog(null, message,
				"Vérifiez vos informations", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			temp2.setLine(temp.getLine());
			temp2.setStation(temp.getStation());
			temp2.setPeriods(temp.getPeriods());
			
			//modification de la timetable
			controller.alterTimetable(temp2,positionInList);
		}
	}
	
	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "noTimetable" :
			JOptionPane.showMessageDialog(null,"Erreur, horaire non existant");
		break;
		}
	}
	
	/**
	 * Actualise la liste déroulante contenant les noms des stations en 
	 * fonction de la ligne sélectionnée
	 */
	@Override
    public void actionPerformed(ActionEvent e) {
		HashMap<String, Line> map = new HashMap<String, Line>();
		map = controller.getLinesMap();
		Line temp = map.get(line.getSelectedItem().toString());
		
        DefaultComboBoxModel model = new DefaultComboBoxModel(temp.getStationsNames());
        station.setModel(model);
    }
	

}
