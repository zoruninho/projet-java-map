package view;

import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.omg.CORBA.FREE_MEM;

import controller.Controller;

/**
 * La classe <b>MenuBar</b>hérite de JMenuBar et implémente
 * ActionListener. Elle permet de créer une barre de menu et de la 
 * remplir d'éléments
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 *
 */
public class MenuBar extends JMenuBar implements ActionListener{
	
	private final Controller controller;
	
	/**
	 * Initialise une MenuBar avec tous les éléments qui en font partie
	 * 
	 * @param controller le controlleur principal de l'application
	 */
	public MenuBar(final Controller controller){
				
		this.setBackground(new Color(0xFF, 0xFF, 0xFF));
		this.controller=controller;
		
		/*     MENU FICHIER     */
		JMenu menuFichier = new JMenu("Fichier");
		
		JMenuItem fichierOuvrir = new JMenuItem("ouvrir");
		JMenuItem fichierSauvegarder = new JMenuItem("sauvegarder");
		JMenuItem fichierNouveau = new JMenuItem("nouveau");
		JMenuItem fermerApp = new JMenuItem("fermer");

		fichierOuvrir.addActionListener(this);
		fichierSauvegarder.addActionListener(this);
		fichierNouveau.addActionListener(this);
		fermerApp.addActionListener(this);
		
		fichierSauvegarder.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_DOWN_MASK));
		fichierOuvrir.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_DOWN_MASK));
		fichierNouveau.setAccelerator(KeyStroke.getKeyStroke('N', InputEvent.CTRL_DOWN_MASK));
		fermerApp.setAccelerator(KeyStroke.getKeyStroke('W', InputEvent.CTRL_DOWN_MASK));
		
		menuFichier.add(fichierNouveau);
		menuFichier.add(fichierOuvrir);
		menuFichier.add(fichierSauvegarder);
		menuFichier.add(fermerApp);
		
		/*    MENU LIGNES     */
		
		JMenu menuLignes = new JMenu("Lignes");
		MenuItem ml1 = new MenuItem("Ajouter des lignes");
		MenuItem ml2 = new MenuItem("Modifier des lignes");
		MenuItem ml3 = new MenuItem("Ajouter des stations à une ligne");
		MenuItem ml4 = new MenuItem("Supprimer une ligne");
		
		menuLignes.add(ml1);
		menuLignes.add(ml2);
		menuLignes.add(ml3);
		menuLignes.add(ml4);
		
		ml1.addActionListener(this);
		ml2.addActionListener(this);
		ml3.addActionListener(this);
		ml4.addActionListener(this);
		
		ml1.setAccelerator(KeyStroke.getKeyStroke('L', InputEvent.ALT_DOWN_MASK));
		
		/*    MENU STATIONS    */
		
		JMenu menuStations = new JMenu("Stations");
		MenuItem ms1 = new MenuItem("Ajouter des stations");
		MenuItem ms2 = new MenuItem("Modifier des stations");
		MenuItem ms3 = new MenuItem("Supprimer des stations");
		
		menuStations.add(ms1);
		menuStations.add(ms2);
		menuStations.add(ms3);
		
		ms1.addActionListener(this);
		ms2.addActionListener(this);
		ms3.addActionListener(this);
		
		ms1.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.ALT_DOWN_MASK));
		
		/*    MENU HORAIRES    */
		
		JMenu menuHoraires = new JMenu("Horaires");
		MenuItem mh1 = new MenuItem("Ajouter des horaires");
		MenuItem mh2 = new MenuItem("Modifier des horaires");
		MenuItem mh3 = new MenuItem("Supprimer des horaires");
		
		menuHoraires.add(mh1);
		menuHoraires.add(mh2);
		menuHoraires.add(mh3);
		
		mh1.addActionListener(this);
		mh2.addActionListener(this);
		mh3.addActionListener(this);
		
		mh1.setAccelerator(KeyStroke.getKeyStroke('H', InputEvent.ALT_DOWN_MASK));
		
		this.add(menuFichier);
		this.add(menuLignes);
		this.add(menuStations);
		this.add(menuHoraires);
	}
	



	/**
	 * Actions à effectuer en cas de clic sur un des éléments du menu
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()){
			case "Ajouter des lignes" :
				InsertLines insertLines = new InsertLines(controller);
				insertLines.createPopup();
				break;
			case "Modifier des lignes" :
				AlterLines alterLines = new AlterLines(controller);
				alterLines.choicePopup();
				break;
			case "Ajouter des stations à une ligne" :
				InsertStationsInLine insertstationsinline = new InsertStationsInLine(controller);
				insertstationsinline.createPopup();
				break;
			case "Supprimer une ligne" :
				DeleteLine deleteline = new DeleteLine(controller);
				deleteline.choicePopup();
				break;
			case "Ajouter des stations" :
				InsertStations insertStations = new InsertStations(controller);
				insertStations.createPopup();
				break;
			case "Modifier des stations" :
				AlterStations alterStations = new AlterStations(controller);
				alterStations.choicePopup();
				break;
			case "Supprimer des stations" :
				DeleteStation deletestation = new DeleteStation(controller);
				deletestation.choicePopup();
				break;
			case "Ajouter des horaires" :
				InsertTimetable insertTimetable = new InsertTimetable(controller);
				insertTimetable.createPopup();
				break;
			case "Modifier des horaires" :
				AlterTimetable alterTimetable = new AlterTimetable(controller);
				alterTimetable.choicePopup();
				break;
			case "Supprimer des horaires" :
				DeleteTimetables deletetimetables = new DeleteTimetables(controller);
				deletetimetables.choicePopup();
				break;
			case "ouvrir":
				FileDialog fileDialog = new FileDialog(new JFrame(), "charger", FileDialog.LOAD);
				fileDialog.show();
				
				// Si on a sélectionné un fichier
				if (fileDialog.getFile() != null && fileDialog.getDirectory() != null)
				{
					String fileName = fileDialog.getDirectory() + fileDialog.getFile();
					
					controller.setFileName(fileName);
				}	
				break;
			case "sauvegarder":
				controller.save();
				break;
			case "nouveau":
				fileDialog = new FileDialog(new JFrame(), "sauvegarder", FileDialog.SAVE);
				fileDialog.show();
				
				if (fileDialog.getFile() != null && fileDialog.getDirectory() != null)
				{
					controller.createEmptyJson(fileDialog.getDirectory() + fileDialog.getFile());
				}
				break;
			case "fermer":
				System.exit(0);
				break;
			default :
				System.out.println("Cas non géré !");
		}
			
		
	}

}
