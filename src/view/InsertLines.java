package view;

import controller.Controller;
import java.awt.Choice;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import model.Line;
import model.Station;
import model.TransportType;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;

/**
 * La classe <b>InsertLines</b> hérite de la classe abstraite InsertDatas
 * et permet d'ajouter une ligne
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Line
 * 
 */
public class InsertLines extends InsertDatas {
	
	Line temp;

	/**
	 * Initialise une InsertLines avec une ligne vide
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public InsertLines(Controller controller) {
		super();
		this.temp=new Line();
		this.controller=controller;
	}

	/**
	 * Lance une pop-up permettant de créer une nouvelle ligne
	 */
	@Override
	public void createPopup() {

		//Liste déroulante contenant les types de transports possibles
		Choice transportType = new Choice();
		transportType.addItem(TransportType.BUS.toString());
		transportType.addItem(TransportType.METRO.toString());
		transportType.addItem(TransportType.TRAMWAY.toString());

		//Champ de texte vide
		JTextField name = new JTextField();
		
		//Crée un message contenant des chaînes de caractères, la liste déroulante 
		//ainsi que le champ de texte
		Object[] message = new Object[] { "Nom de la ligne : ", name,
				"Type de la ligne : ", transportType };
		
		//Crée la pop-up contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Ajouter une ligne", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			String tempType = transportType.getSelectedItem();
			if(tempType=="BUS")
			temp.setTransportType(TransportType.BUS);
			else if(tempType=="METRO")
			temp.setTransportType(TransportType.METRO);
			else if(tempType=="TRAMWAY")
			temp.setTransportType(TransportType.TRAMWAY);
			
			//On vérifie que le champ name n'est pas vide
			if(name.getText().trim().isEmpty())
			{
				errorPopup("absenceNomLigne");
				return;
			}
			temp.setName(name.getText());
			if(controller.checkLineName(temp.getName()))
				errorPopup("nomLigne");
			else
				insertTerminus();
		}
	}

	/**
	 * Lance une pop-up permettant d'insérer une station de départ et d'arrivée 
	 */
	public void insertTerminus() {

		String[] stations = controller.getStationsNames();
		
		//Liste déroulante avec système d'auto-complétion contenant les
		//noms des stations
		JComboBox terminus1 = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        terminus1, GlazedLists.eventListOf(stations));
		
		JComboBox terminus2 = new JComboBox();
		AutoCompleteSupport support2 = AutoCompleteSupport.install(
		        terminus2, GlazedLists.eventListOf(stations));

		Station station1 = new Station();
		Station station2 = new Station();
		
		//Message contenant deux chaînes de caractères et les deux listes déroulantes
		Object[] message = new Object[] { "Station de départ : ", terminus1,
				"Station d'arrivée : ", terminus2 };
		
		//Lance la pop-up de modification contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Ajouter des terminus", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			station1.setName(terminus1.getSelectedItem().toString());
			station2.setName(terminus2.getSelectedItem().toString());
			station1=controller.copyStation(station1);
			station2=controller.copyStation(station2);
			if(station1!=null && station2!=null)
			{
			temp.getStations().add(station1);
			temp.getStations().add(station2);
			
			confirmPopup();
			}
			else 
				errorPopup("station");
		}
	}

	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "station" :
			JOptionPane.showMessageDialog(null,"Erreur, station non existante");
		break;
		case "nomLigne" :
			JOptionPane.showMessageDialog(null,"Erreur, ligne déjà existante");
		break;
		case "absenceNomLigne" :
			JOptionPane.showMessageDialog(null,"Erreur, il faut entrer un nom de ligne");
		break;
		}
	}

	/**
	 * Lance la pop-up permettant de confirmer l'insertion des données
	 */
	@Override
	public void confirmPopup() {
		//Message contenant des chaînes de caractères et les données de la ligne
		Object[] message = new Object[] { "Nom de la ligne : "+temp.getName(), "Type de transport : "+temp.getTransportType(),
				"Station de départ : "+temp.getStations().get(0).getName(), "Station d'arrivée : "+temp.getStations().get(1).getName()
				};
		
		//On change le nom du bouton "OK" par "Valider"
		UIManager.put("OptionPane.okButtonText", "Valider"); 
		
		//Lance la pop-up de confirmation
		int r = JOptionPane.showConfirmDialog(null, message,
				"Vérifiez vos informations", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			//Insère la ligne
			controller.insertFilledLine(temp);
		}
	}

}
