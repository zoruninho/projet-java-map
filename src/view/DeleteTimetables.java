package view;

import java.awt.Choice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import model.Line;
import model.Period;
import model.Station;
import model.Timetable;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import controller.Controller;

/**
 * La classe <b>DeleteTimetables</b> hérite de la classe abstraite DeleteDatas
 * et permet de supprimer une timetable
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Timetable
 * 
 */
public class DeleteTimetables extends DeleteDatas implements ActionListener{

	JComboBox line;
	JComboBox station;
	JComboBox timetables;
	Timetable temp;
	Timetable temp2;
	int positionInList;
	int numberPeriod;
	
	/**
	 * Initialise une DeleteTimetable avec deux timetables vides
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public DeleteTimetables(Controller controller) {
		super();
		this.temp = new Timetable();
		this.temp2 = new Timetable();
		this.controller = controller;
	}
	
	/**
	 * Lance une pop-up permettant de choisir la timetable à supprimer
	 */
	@Override
	public void choicePopup() {
		String[] lines = controller.getLinesNames();
		
		//Crée une liste déroulante avec système d'autocomplétion contenant
		//les noms des lignes
		line = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        line, GlazedLists.eventListOf(lines));
		
		//Ajoute un listener sur la liste déroulante qui permet d'actualiser
		//la liste déroulante "station" en fonction du choix de ligne
		line.addActionListener(this);
		
		//Crée une liste déroulante vierge
		station = new JComboBox();
		
		//Liste déroulante contenant "semaine" ou "week-end"
		Choice period = new Choice();
		period.addItem("Semaine");
		period.addItem("Week-end");
		
		Line tempLine = new Line();
		Station tempStation = new Station();
		Period tempPeriod = new Period();
		
		//Crée un message contenant des chaînes de caractères et les listes déroulantes
		Object[] message = new Object[] { "Ligne : ", line,
				"Station : ", station, "Période : " , period};
		
		//Crée la pop-up contenant le message 
		int r = JOptionPane.showConfirmDialog(null, message,
				"Modifier des horaires", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			tempLine.setName(line.getSelectedItem().toString());
			tempStation.setName(station.getSelectedItem().toString());
			
			if(period.getSelectedItem()=="Semaine")
				numberPeriod=0;
			else
				numberPeriod=1;
			
			temp.setLine(tempLine);
			temp.setStation(tempStation);
			
			temp=controller.copyTimetable(temp);
			
			//On vérifie que la timetable existe bien
			if(temp==null)
			{
				errorPopup("noTimetable");
				return;
			}
			temp=new Timetable(temp);
			
			//Récupère la position de la timetable dans la liste de timetables
			positionInList=controller.positionInListTimetable(temp);
			
			deletePopup();
		}
	}

	/**
	 * Lance la pop-up permettant de supprimer un horaire ou tous les horaires
	 */
	public void deletePopup() {
		
		Object options[] = new Object[4];
		options[0]="Supprimer et continuer";
		options[1]="Supprimer tous les horaires";
		options[2]="Finaliser";
		options[3]="Annuler";
		
		//Liste déroulante contenant les horaires de la timetable
		timetables = new JComboBox();
		DefaultComboBoxModel model = new DefaultComboBoxModel(temp.getPeriods().get(numberPeriod).getDates());
        timetables.setModel(model);
		
		Object message = new Object();
		message=timetables;
		
		//Crée la pop-up contenant le message
		int choice = JOptionPane.showOptionDialog(null, message,
				"Supprimer des horaires", JOptionPane.PLAIN_MESSAGE,
				JOptionPane.CANCEL_OPTION, null, options, options[0]);
		
		//Lance une action en fonction du bouton sur lequel l'utilisateur
		//a cliqué
		switch (choice)
		{
			case 0 :
				//Supprime un horaire
				temp.getPeriods().get(numberPeriod).removePasse(timetables.getSelectedItem().toString());
				deletePopup();
			break;
			case 1 :
				//Supprime tous les horaires
				temp.getPeriods().get(numberPeriod).getPasses().clear();
				confirmPopup();
			break;
			case 2 :
				confirmPopup();
			break;
			case 3 :
				return;
		}
	}
	
	/**
	 * Lance la pop-up permettant de confirmer la suppression des données
	 */
	public void confirmPopup() {
		temp2=controller.copyTimetable(temp2);
		String[] dates = temp.getPeriods().get(numberPeriod).getDates();
		String horaires = new String();
		
		horaires+="Horaires : \n";
		for(int i=0;i<dates.length;i++)
		{
			horaires+=dates[i];
			horaires+="\n";
		}
		
		//Message contenant des chaînes de caractères, le nom de la ligne et de la station
		//ainsi que la période liées à la timetable à supprimer
		Object[] message = new Object[] { "Nom de la ligne : "+temp.getLine().getName(),
				"Nom de la station : "+temp.getStation().getName(),
				"Période : "+temp.getPeriods().get(numberPeriod).getLabel(),
				horaires
				};
		
		//On change le nom du bouton "OK" par "Valider"
		UIManager.put("OptionPane.okButtonText", "Valider");  
		
		//Lance la pop-up de suppression contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Vérifiez vos informations", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			temp2.setLine(temp.getLine());
			temp2.setStation(temp.getStation());
			temp2.setPeriods(temp.getPeriods());
			//On supprime la timetable
			controller.alterTimetable(temp2,positionInList);
		}
	}
	
	/**
	 * Actualise la liste déroulante contenant les noms des stations en 
	 * fonction de la ligne sélectionnée
	 */
	@Override
    public void actionPerformed(ActionEvent e) {
		HashMap<String, Line> map = new HashMap<String, Line>();
		map = controller.getLinesMap();
		Line temp = map.get(line.getSelectedItem().toString());
		
        DefaultComboBoxModel model = new DefaultComboBoxModel(temp.getStationsNames());
        station.setModel(model);
    }
	
	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "noTimetable" :
			JOptionPane.showMessageDialog(null,"Erreur, horaire non existant");
		break;
		}
	}

}
