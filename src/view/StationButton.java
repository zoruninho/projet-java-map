package view;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import model.Line;
import model.Station;
import model.Timetable;
import controller.Controller;

/**
 * La classe <b>StationButton</b> permet de créer des boutons représentant
 * des stations sur la carte
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 */
public class StationButton {
	Controller controller;
	
	Station station;
	Point initialPosition;
	
	/**
	 * Initialise un StationButton avec un panel et une Station
	 * 
	 * @param parent le panel dans lequel sera implémenté le bouton
	 * @param station la station qui sera représentée par le bouton
	 */
	public StationButton(final PersonnalPanel parent, final Station station)
	{
		this.controller = parent.controller;
		this.station = station;
		
		int x = parent.getWidth();
		int y = parent.getHeight();
		
		//Taille du bouton adaptée en fonction de la taille de la fenêtre
		int taille = Math.min(x, y)/100;
		
		//Crée un bouton et le place en fonction de la longitude et latitude de la
		//station
		Button button = new Button(station.getName(),
				(int) (parent.xMid - (parent.longMid - station.getLongitude())*parent.scalingX),
				(int) (parent.yMid + (parent.latMid - station.getLatitude())*parent.scalingY),
				taille,
				taille,
				new Color(0x20, 0x70, 0xA0),
				parent.controller);
		button.addMouseListener(new MouseListener() {
			
			//Change la longitude et la latitude de la station après le drop d'un drag and drop
			@Override
			public void mouseReleased(MouseEvent e) {
				
				if(e.isShiftDown() && initialPosition != null)
				{
					station.setLongitude(station.getLongitude() + (e.getX() - initialPosition.x) / parent.scalingX);
					station.setLatitude(station.getLatitude() - (e.getY() - initialPosition.y) / parent.scalingY );
					
					((Button)e.getSource()).updateUI();
				}
				
				initialPosition = null;
			}
			
			//En cas de clic sur la souris
			@Override
			public void mousePressed(MouseEvent e) {
				
				//Si clic gauche simple
				if(e.getButton() == MouseEvent.BUTTON1)
				{
					//Si la touche shift est enfoncée
					if(e.isShiftDown())
					{
						//Début du drag and drop
						initialPosition = e.getPoint();
					}
					else
					{
						selectLines();
					}
				}
				//Si clic droit simple
				else if(e.getButton() == MouseEvent.BUTTON3)
				{
					//Nouveau menu permettant de lancer la modification ou
					//la suppression de la station
					JPopupMenu popUp = new JPopupMenu();
					
					JMenuItem update = new JMenuItem("modifier");
					JMenuItem delete = new JMenuItem("supprimer");
					
					update.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							
							AlterStations alterStation = new AlterStations(controller);
							alterStation.setStation(StationButton.this.station);
							alterStation.alterPopup();
														
							parent.requestFocus();
						}
					});
					
					delete.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent arg0) {
							
							DeleteStation deleteStation = new DeleteStation(controller);
							deleteStation.setStation(StationButton.this.station);
							deleteStation.confirmPopup();
							
							parent.requestFocus();
						}
					});
					
					popUp.add(update);
					popUp.add(delete);
					
					popUp.show(e.getComponent(), e.getX(), e.getY());
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {}
		});
		
		
		parent.add(button);
	}
	
	/**
	 * Lance une pop-up permettant de choisir une des lignes reliées
	 * à la station si la station est reliée à une ou des lignes
	 */
	public void selectLines(){
		Station toShow = this.station;

		String contenu = new String();
		contenu = "Sélectionner la ligne à afficher.\n";

		LinkedList<Line> lines = controller.getMap().searchLineOnStations(
				toShow);
		int size = lines.size();

		//Si la station est reliée à au moins une ligne
		if (size > 0) {
			Object objLines[] = new Object[size];

			for (int i = 0; i < size; i++)
				objLines[i] = lines.get(i).getName().toString();

			//Crée la pop-up contenant les boutons des lignes et une chaîne de caractères
			int option = JOptionPane.showOptionDialog(null, contenu,
					toShow.getName(), JOptionPane.PLAIN_MESSAGE,
					JOptionPane.CANCEL_OPTION, null, objLines, objLines[0]);

			//Si l'utilisateur a choisi une des lignes
			if (option >= 0) {
				Line converted = this.controller.getMap().convertToLine(
						objLines[option]);
				
				this.choosePeriod(toShow, converted);
			}
		}
	}

	/**
	 * Lance une pop-up qui permet de choisir la période sur laquelle
	 * on veut avoir des informations
	 * 
	 * @param toShow la station reliée au StationButton
	 * @param converted La ligne sélectionnée
	 */
	private void choosePeriod(Station toShow, Line converted) {
		
		//Liste déroulante contenant "semaine" ou "week-end"
		Choice period = new Choice();
		period.addItem("Semaine");
		period.addItem("Week-end");

		//Message contenant une chaîne de caractères avec l'horaire du prochain passage
		//et la période dans laquelle est situé cet horaire
		Object[] message = new Object[] {
				"Prochain passage : "
						+ this.controller.getMap().getNextArrival(
								converted, toShow) + "\nPériode : ",
				period };

		//Crée la pop-up contenant le message 
		int r = JOptionPane.showConfirmDialog(null, message,
				"Choisissez votre période",
				JOptionPane.OK_CANCEL_OPTION);

		String label = new String();

		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			label = period.getSelectedItem();
			this.displaySchedule(label, toShow, converted);
		}
	}

	/**
	 * Affiche les horaires liés à la station, pour la ligne sélectionnée et la période
	 * choisie
	 * 
	 * @param label une chaîne de caractères identifiant la période
	 * @param toShow la station reliée au StationButton
	 * @param converted la ligne sélectionnée
	 */
	private void displaySchedule(String label, Station toShow, Line converted) {

		String contenu;
		int size;

		LinkedList<Timetable> schedule = this.controller.getMap()
				.searchSchedule(converted, toShow);

		contenu = "";
		size = schedule.size();

		for (int i = 0; i < size; i++) {
			contenu += schedule.get(i).periodsToString(label) + "\n";
		}

		if (contenu.equals("\n") || contenu.equals(""))
			contenu = "Aucun horaire disponible !";

		//Affiche un message contenant les horaires
		JOptionPane.showMessageDialog(null, contenu,
				converted.getName() + " - " + toShow.getName()
						+ " - " + label, JOptionPane.PLAIN_MESSAGE);
	}
}