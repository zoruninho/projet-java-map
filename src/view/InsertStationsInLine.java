package view;

import java.util.Collections;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import model.Line;
import model.Station;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import controller.Controller;

/**
 * La classe <b>InsertStationsInLine</b> hérite de la classe abstraite InsertDatas
 * et permet d'ajouter une station dans une ligne
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Station
 * @see Line
 * 
 */
public class InsertStationsInLine extends InsertDatas{

	Line temp;
	int positionInList;
	int positionStation;
	
	/**
	 * Initialise une InsertStationsInLine avec une ligne vide
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public InsertStationsInLine(Controller controller) {
		super();
		this.temp=new Line();
		this.controller=controller;
	}
	
	/**
	 * Lance une pop-up permettant de choisir la ligne dans laquelle
	 * on va insérer une ou des stations
	 */
	@Override
	public void createPopup() {
		String[] lines = controller.getLinesNames();
		
		//Crée une liste déroulante avec système d'autocomplétion contenant
		//le nom des lignes
		JComboBox line = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        line, GlazedLists.eventListOf(lines));
		
		//Crée un message contenant une chaîne de caractères et la liste déroulante
		Object[] message = new Object[] { "Ligne : ", line};
		
		//Crée la pop-up contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Choisir la ligne à modifier", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			temp.setName(line.getSelectedItem().toString());
			temp=new Line(controller.copyLine(temp));
			
			//Récupère la position de la ligne dans la liste de lignes
			positionInList=controller.positionInListLine(temp);
			
			addStationsPopup();
		}
	}

	/**
	 * Lance une pop-up qui permet d'ajouter une station après une 
	 * autre station dans une ligne
	 */
	public void addStationsPopup(){
		String lineStations = "Ordre des stations : \n";
		
		//Affiche les stations de la ligne dans l'ordre départ->arrivée
		for(int i=0;i<temp.getStations().size();i++)
		{
			if(i==(temp.getStations().size()-1))
			{
				lineStations+=temp.getStations().get(i).getName();
				lineStations+="\n\n";
			}
			else
			{
				lineStations+=temp.getStations().get(i).getName();
				lineStations+="->";
			}
		}
		
		String[] stations = controller.getStationsNames();
		
		//Liste déroulante avec système d'auto-complétion contenant les
		//noms des stations
		JComboBox stationToAdd = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        stationToAdd, GlazedLists.eventListOf(stations));
		
		String[] stationsOfLineNames = temp.getStationsNamesExceptLast();
		JComboBox stationsOfLine = new JComboBox();
		AutoCompleteSupport support2 = AutoCompleteSupport.install(
		        stationsOfLine, GlazedLists.eventListOf(stationsOfLineNames));
		
		//Crée un message contenant des chaînes de caractères ainsi que les listes déroulantes
		Object[] message = new Object[] { lineStations,
				"Station à ajouter", stationToAdd,
				"Ajouter après la station : ", stationsOfLine};
		
		//Remplace le texte du bouton "YES" par "Valider et continuer"
		UIManager.put("OptionPane.yesButtonText", "Valider et continuer");  
		
		//Remplace le texte du bouton "YES" par "Finaliser"
		UIManager.put("OptionPane.noButtonText", "Finaliser");  
		
		//Crée la pop-up contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Choisir une station à ajouter", JOptionPane.YES_NO_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider et continuer" ou "Finaliser"
		if (r == JOptionPane.YES_OPTION || r == JOptionPane.NO_OPTION)
		{
			if(r == JOptionPane.YES_OPTION)
			{
				Station tempStation = new Station();
				
				//Vérifie qu'il y a bien une station de sélectionnée
				if(stationToAdd.getSelectedItem()==null)
				{
					errorPopup("noStationToAdd");
					return;
				}
				
				tempStation.setName(stationToAdd.getSelectedItem().toString());
				
				tempStation=controller.copyStation(tempStation);
				
				//Vérifie que la station existe
				if(tempStation==null)
				{
					errorPopup("badStationToAdd");
					return;
				}
				
				//Vérifie qu'il y a bien une station de la ligne de sélectionnée
				if(stationsOfLine.getSelectedItem()==null)
				{
					errorPopup("noStationOfLine");
					return;
				}
				
				//Récupère la position de la station dans la liste de stations de la ligne
				positionStation=temp.getPositionStation(stationsOfLine.getSelectedItem().toString());
				
				//Ajoute la station dans la liste de stations de la ligne
				temp.getStations().add(positionStation,tempStation);
				addStationsPopup();
			}
			else
			{
				confirmPopup();
			}
		}
	}
	
	/**
	 * Lance la pop-up permettant de confirmer l'insertion des données
	 */
	@Override
	public void confirmPopup() {
		String lineStations = "Ordre des stations : \n";
		for(int i=0;i<temp.getStations().size();i++)
		{
			if(i==(temp.getStations().size()-1))
			{
				lineStations+=temp.getStations().get(i).getName();
				lineStations+="\n\n";
			}
			else
			{
				lineStations+=temp.getStations().get(i).getName();
				lineStations+="->";
			}
		}
		
		//Message contenant une chaîne de caractères
		Object[] message = new Object[] { lineStations};
		
		//Remplace le texte du bouton "YES" par "Valider"
		UIManager.put("OptionPane.okButtonText", "Valider");
		
		//Lance la pop-up de confirmation
		int r = JOptionPane.showConfirmDialog(null, message,
				"Vérifiez vos informations", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			//Modifie la ligne avec les nouvelles stations d'intégrées
			controller.alterLine(temp,positionInList);
		}
	}

	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "noStationToAdd" :
			JOptionPane.showMessageDialog(null,"Erreur, pas de station sélectionnée");
		break;
		case "badStationToAdd" :
			JOptionPane.showMessageDialog(null,"Erreur, station inexistante");
		break;
		case "noStationOfLine" :
			JOptionPane.showMessageDialog(null,"Erreur, il faut sélectionner une station appartenant à la ligne");
		break;
		}
	}
}
