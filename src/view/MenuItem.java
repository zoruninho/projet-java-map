package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import model.Line;
import model.Station;

/**
 * La classe <b>MenuItem</b> hérite de la classe JMenuItem
 * et implémente ActionListener et permet de créer un élément de menu
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 */
public class MenuItem extends JMenuItem implements ActionListener {
	
	/**
	 * Initialise un MenuItem avec un nom et un raccourcis
	 * 
	 * @param pName une chaîne de caractères contenant le nom du MenuItem
	 * @param pShortcut un entier correspondant à un KeyEvent
	 */
	public MenuItem(String pName, int pShortcut){
		super(pName, pShortcut);
		
		this.addActionListener(this);
	}
	
	/**
	 * Initialise un MenuItem avec un nom 
	 * 
	 * @param pName une chaîne de caractères contenant le nom du MenuItem
	 */
	public MenuItem(String pName){
		this(pName, -1);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {}

}
