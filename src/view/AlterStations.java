package view;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import model.Line;
import model.Station;
import model.Timetable;
import controller.Controller;

/**
 * La classe <b>AlterStations</b> hérite de la classe abstraite AlterDatas
 * et permet de modifier les informations d'une station
 * 
 * @version 1.0
 * @author Pierre Facq
 * @author Alan Grente-Lequertier
 * @author Alexandre Paris-Vergne
 * @author Thomas Sileghem
 * 
 * @see Station
 * 
 */
public class AlterStations extends AlterDatas {

	Station temp;
	Station temp2;
	int positionInList;

	/**
	 * Initialise une AlterStations avec deux stations vides
	 * 
	 * @param controller le controlleur principal du logiciel
	 */
	public AlterStations(Controller controller) {
		super();
		this.temp = new Station();
		this.temp2 = new Station();
		this.controller = controller;
	}

	/**
	 * Lance une pop-up permettant de choisir la ligne à modifier
	 */
	@Override
	public void choicePopup() {
		String[] stations = controller.getStationsNames();
		
		//Crée une liste déroulante avec système d'autocomplétion contenant
		//le nom des station
		JComboBox station = new JComboBox();
		AutoCompleteSupport support = AutoCompleteSupport.install(
		        station, GlazedLists.eventListOf(stations));
		
		//Crée un message contenant une chaîne de caractères et la liste déroulante
		Object[] message = new Object[] { "Station : ", station};
		
		//Crée la pop-up contenant le message 
		int r = JOptionPane.showConfirmDialog(null, message,
				"Choisir la station à modifier", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			temp.setName(station.getSelectedItem().toString());
			temp2.setName(station.getSelectedItem().toString());
			alterPopup();
		}
	}
	
	/**
	 * Lorsque la modification est lancée depuis un clic droit, place le nom
	 * de la station sélectionnée par le clic droit dans les deux stations temporaires
	 * 
	 * @param station la station concernée par le clic droit
	 */
	public void setStation(Station station)
	{
		this.temp = new Station();
		temp.setName(station.getName());
		temp2.setName(station.getName());
	}

	/**
	 * Lance la pop-up permettant de modifier les informations de la ligne sélectionnée
	 */
	@Override
	public void alterPopup() {
		temp=controller.copyStation(temp);
		
		//Vérifie que la station existe bien
		if(temp==null)
		{
			errorPopup("noStation");
			return;
		}
		temp=new Station(temp);
		
		//Récupère la position de la station dans la liste de stations
		positionInList=controller.positionInListStation(temp);
		
		//Champ de texte modifiable contenant le nom de la station
		JTextField name = new JTextField(temp.getName());
		
		//Champ de texte modifiable contenant le nom de la ville
		//dans laquelle est située la station
		JTextField cityName = new JTextField(temp.getCityName());
		
		//Message contenant les 2 champs de texte
		Object[] message = new Object[] { name, cityName};
		
		//Lance la pop-up de modification contenant le message
		int r = JOptionPane.showConfirmDialog(null, message,
				"Modifiez les valeurs", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "OK"
		if (r == JOptionPane.OK_OPTION) {
			temp.setName(name.getText());
			temp.setCityName(cityName.getText());

			confirmPopup();
		}
	}

	/**
	 * Lance la pop-up permettant de confirmer la modification des données
	 */
	@Override
	public void confirmPopup() {
		temp2=controller.copyStation(temp2);
		
		//Message contenant des chaînes de caractères et les nouvelles données de la station
		Object[] message = new Object[] {
				"Nom de la station : " + temp.getName(),
				"Ville : " + temp.getCityName() };
		
		//On change le nom du bouton "OK" par "Valider"
		UIManager.put("OptionPane.okButtonText", "Valider");
		
		//Lance la pop-up de confirmation
		int r = JOptionPane.showConfirmDialog(null, message,
				"Vérifiez vos informations", JOptionPane.OK_CANCEL_OPTION);
		
		//Si l'utilisateur a cliqué sur le bouton "Valider"
		if (r == JOptionPane.OK_OPTION) {
			temp2.setName(temp.getName());
			temp2.setCityName(temp.getCityName());
			
			//Modification de la station
			controller.alterStation(temp2,positionInList);
		}
	}
	
	/**
	 * Lance une pop-up avec un message en cas d'erreur
	 * @param error une chaîne de caractères identifiant l'erreur
	 */
	private void errorPopup(String error) {
		switch (error)
		{
		case "noStation" :
			JOptionPane.showMessageDialog(null,"Erreur, station non existante");
		break;
		}
	}
}